function addText(a, b) {
			document.getElementById(a).value += b;
			document.getElementById(a).focus();
	};
$(function() {


/*Note: At present, using .val() on textarea elements strips carriage return characters from the browser-reported value. When this value is sent to the server via XHR however, carriage returns are preserved (or added by browsers which do not include them in the raw value). A workaround for this issue can be achieved using a valHook as follows:*/

	$.valHooks.textarea = {
		get: function( elem ) {
			return elem.value.replace( /\r?\n/g, "\r\n" );
		}
	};
	$("a").click(function(e) {
		if($(this).attr("id")!="mathe"){
			e.preventDefault();
		}
	});

//  Buecher

	$("#newbook").click(function() {
		$("#output").empty();
		$(".added").remove();
		$("#main").hide(1000);
		$("#bc1").html("Neues Buch");
		$(".breadcrumbx").addClass("hidden");
		$.ajax({
				type        : "POST",
				url         : "backend.php",
				data        : {"action":"newbook"},
				dataType    : "json"
			})
		.done(function(e) {
			$("#output").html(e.html);
			$(document).ready( function() {
				$("#title").keyup(function() {
					$("#titleresult").remove();
					$ts_fd = $("#title").val();
					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : {
							"action":"titlesearch",
							"value" : $ts_fd
						},
						dataType    : "json"
					})
					.done(function(e) {
						if (e.html.length >135)
							$("#title-group").append("<div id='titleresult' class='added'>" + e.html + "</div>");
					});
				});
				$("#author").keyup(function() {
					$("#authorresult").remove();
					$as_fd = $("#author").val();
					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : {
							"action":"authorsearch",
							"value" : $as_fd
						},
						dataType    : "json"
					})
					.done(function(e) {
						if (e.html.length >135)
							$("#author-group").append("<div id='authorresult' class='added'>" + e.html + "</div>");
					});
				});
				$(document).on("click", "#savecontbook", function (e) {
					e.preventDefault();
					if ( $( "#title" ).val().length == 0 ) {
						$("#title-group").after("<div class='help-block'>Bitte sicherstellen, dass der Titel ausgef&uuml;llt ist</div>");
					} else {
						var newbook_formdata = {
							"action" 		: "newbooksubmit",
							"title"			: $('input[name=title]').val(),
							"author"		: $('input[name=author]').val(),
							"publisher"		: $('input[name=publisher]').val(),
							"pub_country"	: $('input[name=pub_country]').val(),
							"price"			: $('input[name=price]').val(),
							"note"			: $('textarea[name=note]').val(),
							"pages"			: $('input[name=pages]').val(),
							"series"		: $('input[name=series]').val(),
							"nr_in_series"	: $('input[name=nr_in_series]').val(),
							"pub_date"		: $('input[name=pub_date]').val(),
							"isbn"			: $('input[name=isbn]').val()
						};

						$.ajax({
							type        : "POST",
							url         : "backend.php",
							data        : newbook_formdata,
							dataType    : "json",
							encode      : true
						})

						.done(function(e) {

							console.log(e);
							$("#output").html(newbook_formdata.title + " id:" + e.id + "<br>");
							var bookid = e.id;
							$.ajax({
								type        : "POST",
								url         : "backend.php",
								data        : {"action":"findreviewer"},
								dataType    : "json"
							})
							.done(function(e) {
								$("#output").append(e.html);
								$("#bc2").html("Rezensenten finden");
								$("#bc2").removeClass("hidden");
								$(document).ready( function() {
									$("#fields").select2({
										placeholder: "...",
										allowClear: true
									});
									$("#output").after("<div id='findreviewerdiv' class='col-sm-10 col-sm-offset-1 added'></div>");

									$("#name").keyup(function() {
										$("#result").remove();
										var findreviewer_formdata = {
											"action" : "findreviewersubmit",
											"fields" : $("select[name=fields]").val(),
											"name"	 : $("input[name=name]").val(),
											"address": $("input[name=address]").val()
										};
										$.ajax({
											type        : "POST",
											url         : "backend.php",
											data        : findreviewer_formdata,
											dataType    : "json",
											encode      : true
										})
										.done(function(e) {
											$("#findreviewerdiv").append("<div id='result' class='added'>" + e.html + "</div>");
											$(document).ready( function() {
												$(".rateit").rateit();
												$.bootstrapSortable();
											});
										})
										.fail(function(e, errorThrown, jqXHR) {
											console.log(e);
											$("#output").html("Oops: <br>" + JSON.stringify(findreviewer_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
										});
									});

									$("#address").keyup(function() {
										$("#result").remove();
										var findreviewer_formdata = {
											"action" : "findreviewersubmit",
											"fields" : $("select[name=fields]").val(),
											"name"	 : $("input[name=name]").val(),
											"address": $("input[name=address]").val()
										};
										$.ajax({
											type        : "POST",
											url         : "backend.php",
											data        : findreviewer_formdata,
											dataType    : "json",
											encode      : true
										})
										.done(function(e) {
											$("#findreviewerdiv").append("<div id='result' class='added'>" + e.html + "</div>");
											$(document).ready( function() {
												$(".rateit").rateit();
												$.bootstrapSortable();
											});
										})
										.fail(function(e, errorThrown, jqXHR) {
											console.log(e);
											$("#output").html("Oops: <br>" + JSON.stringify(findreviewer_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
										});
									});

									$("#fields").change(function() {
										$("#result").remove();
										var findreviewer_formdata = {
											"action" : "findreviewersubmit",
											"fields" : $("select[name=fields]").val(),
											"name"	 : $("input[name=name]").val(),
											"address": $("input[name=address]").val()
										};
										$.ajax({
											type        : "POST",
											url         : "backend.php",
											data        : findreviewer_formdata,
											dataType    : "json",
											encode      : true
										})
										.done(function(e) {
											$("#findreviewerdiv").append("<div id='result' class='added'>" + e.html + "</div>");
											$(document).ready( function() {
												$(".rateit").rateit();
												$.bootstrapSortable();
											});
										})
										.fail(function(e, errorThrown, jqXHR) {
											console.log(e);
											$("#output").html("Oops: <br>" + JSON.stringify(findreviewer_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
										});
									});

									$(document).on("click", ".reviewer", function(e) {
										if(confirm("soll das Buch diesem Rezensenten zugeordnet werden?")){
											var addreview_formdata = {
											"action" : "addreview",
											"bookid" : bookid,
											"revid"	 : $(this).data('reviewerid')
											};
											$.ajax({
												type        : "POST",
												url         : "backend.php",
												data        : addreview_formdata,
												dataType    : "json",
												encode      : true
											})
											.done(function(e) {
												$(".added").remove();
												$("#output").html(bookid + " wurde " + $(this).data('reviewerid').text() + " zugeordnet.");

											})
											.fail(function(e, errorThrown, jqXHR) {
												console.log(e);
												$("#output").html("Oops: beim zuordnen ist was schiefgelaufen<br><br>" + JSON.stringify(addreview_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
											});
										}
									});
								});
							})
							.fail(function( jqXHR, textStatus, errorThrown ) {
								console.log(e);
								$("#output").html("Oops: beim laden der rezensentensuche ist was schiefgelaufen<br><br>" + textStatus + "<br>___<br>" + errorThrown + "<br>___<br>" + jqXHR.responseText);
							});
						})
						.fail(function(e, errorThrown, jqXHR) {
							console.log(e);
							$("#output").html("Oops: Beim speichern des buches ist was schiefgelaufen<br><br>" + newbook_formdata.title + "<br><br>" + e.ans + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR));
						});
					};
				});

				$(document).on("click", "#savebook", function (e) {
					e.preventDefault();
					if ( $( "#title" ).val().length == 0 ) {
						$("#title-group").after("<div class='help-block'>Bitte sicherstellen, dass der Titel ausgef&uuml;llt ist</div>");
					} else {
						var newbook_formdata = {
							"action" 		: "newbooksubmit",
							"title"			: $('input[name=title]').val(),
							"author"		: $('input[name=author]').val(),
							"publisher"		: $('input[name=publisher]').val(),
							"pub_country"	: $('input[name=pub_country]').val(),
							"price"			: $('input[name=price]').val(),
							"note"			: $('textarea[name=note]').val(),
							"pages"			: $('input[name=pages]').val(),
							"series"		: $('input[name=series]').val(),
							"nr_in_series"	: $('input[name=nr_in_series]').val(),
							"pub_date"		: $('input[name=pub_date]').val(),
							"isbn"			: $('input[name=isbn]').val()
						};

						$.ajax({
							type        : "POST",
							url         : "backend.php",
							data        : newbook_formdata,
							dataType    : "json",
							encode      : true
						})

						.done(function(e) {

							console.log(e);
							$("#output").html("Erfolgreich gespeichert: <br>" + newbook_formdata.title + "<br><br>" +  e.success + "<br><br>" +  e.html);
						})
						.fail(function(e, errorThrown, jqXHR) {
							console.log(e);
							$("#output").html("Oops: Beim speichern des buches ist was schiefgelaufen<br><br><br>" + newbook_formdata.title + "<br><br>" + e.ans + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR));
						});
					};
				});
			});
		})
		.fail(function( jqXHR, textStatus, errorThrown ) {
			$("#output").html(textStatus + "<br>___<br>" + errorThrown + "<br>___<br>" + jqXHR.responseText);
		});
	});

	$("#books").click(function() {
		$(".added").remove();
		$("#main").hide(1000);
		$("#bc1").html("Alle B&uuml;cher");
		$(".breadcrumbx").addClass("hidden");
		$.ajax({
				type        : "POST",
				url         : "backend.php",
				data        : {"action":"books"},
				dataType    : "json"
			})
			.done(function(e) {
				$("#output").html(e.html);
				$(document).ready( function() {
					$(".book").off("click").click(function(e) {
						var bookid = $(this).data('bookid');
						$("#output").html(" id:" + bookid + "<br>");
						$.ajax({
							type        : "POST",
							url         : "backend.php",
							data        : {"action":"findreviewer"},
							dataType    : "json"
						})
						.done(function(e) {
							$("#output").append(e.html);
							$("#bc2").html("Rezensenten finden");
							$("#bc2").removeClass("hidden");
							$(document).ready( function() {
								$("#fields").select2({
									placeholder: "...",
									allowClear: true
								});
								$("#output").after("<div id='findreviewerdiv' class='col-sm-10 col-sm-offset-1 added'></div>");

								$("#name").keyup(function() {
									$("#result").remove();
									var findreviewer_formdata = {
										"action" : "findreviewersubmit",
										"fields" : $("select[name=fields]").val(),
										"name"	 : $("input[name=name]").val(),
										"address": $("input[name=address]").val()
									};
									$.ajax({
										type        : "POST",
										url         : "backend.php",
										data        : findreviewer_formdata,
										dataType    : "json",
										encode      : true
									})
									.done(function(e) {
										$("#findreviewerdiv").append("<div id='result' class='added'>" + e.html + "</div>");
										$(document).ready( function() {
											$(".rateit").rateit();
											$.bootstrapSortable();
										});
									})
									.fail(function(e, errorThrown, jqXHR) {
										console.log(e);
										$("#output").html("Oops: <br>" + JSON.stringify(findreviewer_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
									});
								});

								$("#address").keyup(function() {
									$("#result").remove();
									var findreviewer_formdata = {
										"action" : "findreviewersubmit",
										"fields" : $("select[name=fields]").val(),
										"name"	 : $("input[name=name]").val(),
										"address": $("input[name=address]").val()
									};
									$.ajax({
										type        : "POST",
										url         : "backend.php",
										data        : findreviewer_formdata,
										dataType    : "json",
										encode      : true
									})
									.done(function(e) {
										$("#findreviewerdiv").append("<div id='result' class='added'>" + e.html + "</div>");
										$(document).ready( function() {
											$(".rateit").rateit();
											$.bootstrapSortable();
										});
									})
									.fail(function(e, errorThrown, jqXHR) {
										console.log(e);
										$("#output").html("Oops: <br>" + JSON.stringify(findreviewer_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
									});
								});

								$("#fields").change(function() {
									$("#result").remove();
									var findreviewer_formdata = {
										"action" : "findreviewersubmit",
										"fields" : $("select[name=fields]").val(),
										"name"	 : $("input[name=name]").val(),
										"address": $("input[name=address]").val()
									};
									$.ajax({
										type        : "POST",
										url         : "backend.php",
										data        : findreviewer_formdata,
										dataType    : "json",
										encode      : true
									})
									.done(function(e) {
										$("#findreviewerdiv").append("<div id='result' class='added'>" + e.html + "</div>");
										$(document).ready( function() {
											$(".rateit").rateit();
											$.bootstrapSortable();
										});
									})
									.fail(function(e, errorThrown, jqXHR) {
										console.log(e);
										$("#output").html("Oops: <br>" + JSON.stringify(findreviewer_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
									});
								});

								$(document).on("click", ".reviewer", function(e) {
									if(confirm("soll das Buch diesem Rezensenten zugeordnet werden?")){
										var revid = $(this).data('reviewerid');
										var addreview_formdata = {
										"action" : "addreview",
										"bookid" : bookid,
										"revid"	 : revid
										};
										$.ajax({
											type        : "POST",
											url         : "backend.php",
											data        : addreview_formdata,
											dataType    : "json",
											encode      : true
										})
										.done(function(e) {
											$(".added").remove();
											$("#output").html(bookid + " wurde " + revid + " zugeordnet.");

										})
										.fail(function(e, errorThrown, jqXHR) {
											console.log(e);
											$("#output").html("Oops: beim zuordnen ist was schiefgelaufen<br><br>" + JSON.stringify(addreview_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
										});
									}
								});
							});
						})
						.fail(function( jqXHR, textStatus, errorThrown ) {
							console.log(e);
							$("#output").html("Oops: beim laden der rezensentensuche ist was schiefgelaufen<br><br>" + textStatus + "<br>___<br>" + errorThrown + "<br>___<br>" + jqXHR.responseText);
						});
					});
				});
			})
			.fail(function( jqXHR, textStatus, errorThrown ) {
				$("#output").html(textStatus + "<br>___<br>" + errorThrown + "<br>___<br>" + jqXHR.responseText);
			});

	});
	$("#booksadmin").click(function() {
		$("#output").empty();
		$(".added").remove();
		$("#main").hide(1000);
		$("#bc1").html("B&uuml;cherverwaltung");
		$(".breadcrumbx").addClass("hidden");
		$.ajax({
			type        : "POST",
			url         : "backend.php",
			data        : {"action":"booksadmin", "title":"", "author":""},
			dataType    : "json"
		})
		.done(function(e) {
			$("#output").html(e.html2 + e.html);
			$(".rateit").rateit();
		})
		.fail(function( jqXHR, textStatus, errorThrown ) {
			$("#output").html(textStatus + "<br>___<br>" + errorThrown + "<br>___<br>" + jqXHR.responseText);
		});
		$(document).on("keyup", "#titlesearch", function() {
			var findbooks_formdata = {
				"action" : "booksadmin",
				"title"	 : $("input[name=titlesearch]").val(),
				"author" : $("input[name=authorsearch]").val()
			};
			$.ajax({
				type        : "POST",
				url         : "backend.php",
				data        : findbooks_formdata,
				dataType    : "json",
				encode      : true
			})
			.done(function(e) {
				$(".added").remove();
				$("#output").append(e.html);
				$(document).ready( function() {

				});
			})
			.fail(function(e, errorThrown, jqXHR) {
				console.log(e);
				$("#output").html("Oops: <br>" + JSON.stringify(findbooks_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
			});
		});
		$(document).on("keyup", "#authorsearch", function() {
			var findbooks_formdata = {
				"action" : "booksadmin",
				"title"	 : $("input[name=titlesearch]").val(),
				"author" : $("input[name=authorsearch]").val()
			};
			$.ajax({
				type        : "POST",
				url         : "backend.php",
				data        : findbooks_formdata,
				dataType    : "json",
				encode      : true
			})
			.done(function(e) {
				$(".added").remove();
				$("#output").append(e.html);
				$(document).ready( function() {

				});
			})
			.fail(function(e, errorThrown, jqXHR) {
				console.log(e);
				$("#output").html("Oops: <br>" + JSON.stringify(findbooks_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
			});
		});
		$(document).on("click", ".book", function(e) {
			var bookid = $(this).data('bookid');
			var modal = $("#bookmodal");
			$.ajax({
				type        : "POST",
				url         : "backend.php",
				data        : {"action":"bookdata", "bookid":bookid},
				dataType    : "json",
				encode      : true
			})
			.done(function(e) {
				var booktitle = e.title;
				var bookauthor = e.author;
				var bookpublisher = e.publisher;
				var bookpub_country = e.pub_country;
				var bookprice = e.price;
				var booknote = e.note;
				var bookpages = e.pages;
				var bookseries = e.series;
				var booknr_in_series = e.nr_in_series;
				var bookpub_date = e.pub_date;
				var bookisbn = e.isbn;

				modal.find("#title").val(booktitle);
				modal.find("#author").val(bookauthor);
				modal.find("#publisher").val(bookpublisher);
				modal.find("#pub_country").val(bookpub_country);
				modal.find("#price").val(bookprice);
				modal.find("#note").val(booknote);
				modal.find("#pages").val(bookpages);
				modal.find("#series").val(bookseries);
				modal.find("#nr_in_series").val(booknr_in_series);
				modal.find("#pub_date").val(bookpub_date);
				modal.find("#isbn").val(bookisbn);
			})
			.fail(function(e, errorThrown, jqXHR) {
				console.log(e);
				$("#output").html("Oops: <br>" + JSON.stringify(findbooks_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
			});

			$("#updatebook").off("click").click(function (e) {
				var bookupdate_formdata = {
					"action" 		: "bookupdate",
					"bookid"		: bookid,
					"title"			: $('input[name=title]').val(),
					"author"		: $('input[name=author]').val(),
					"publisher"		: $('input[name=publisher]').val(),
					"pub_country"	: $('input[name=pub_country]').val(),
					"price"			: $('input[name=price]').val(),
					"note"			: $('textarea[name=note]').val(),
					"pages"			: $('input[name=pages]').val(),
					"series"		: $('input[name=series]').val(),
					"nr_in_series"	: $('input[name=nr_in_series]').val(),
					"pub_date"		: $('input[name=pub_date]').val(),
					"isbn"			: $('input[name=isbn]').val()
				};
				$.ajax({
					type        : "POST",
					url         : "backend.php",
					data        : bookupdate_formdata,
					dataType    : "json",
					encode      : true
				})
				.done(function(e) {
					console.log(e);
					$("#bookmodal").modal("hide");
				})
				.fail(function(e, errorThrown, jqXHR) {
					console.log(e);
					$("#output").html("Oops: Beim speichern des buches ist was schiefgelaufen<br><br><br>" + bookupdate_formdata.title + "<br><br>" + e.ans + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR));
				});
			});
			$("#findreviewermodal").off("click").click(function (e) {
				$("#output").html(" id:" + bookid + "<br>");
				$.ajax({
					type        : "POST",
					url         : "backend.php",
					data        : {"action":"findreviewer"},
					dataType    : "json"
				})
				.done(function(e) {
					$("#output").append(e.html);
					$("#bc2").html("Rezensenten finden");
					$("#bc2").removeClass("hidden");
					$(document).ready( function() {
						$("#fields").select2({
							placeholder: "...",
							allowClear: true
						});
						$("#output").after("<div id='findreviewerdiv' class='col-sm-10 col-sm-offset-1 added'></div>");

						$("#name").keyup(function() {
							$("#result").remove();
							var findreviewer_formdata = {
								"action" : "findreviewersubmit",
								"fields" : $("select[name=fields]").val(),
								"name"	 : $("input[name=name]").val(),
								"address": $("input[name=address]").val()
							};
							$.ajax({
								type        : "POST",
								url         : "backend.php",
								data        : findreviewer_formdata,
								dataType    : "json",
								encode      : true
							})
							.done(function(e) {
								$("#findreviewerdiv").append("<div id='result' class='added'>" + e.html + "</div>");
								$(document).ready( function() {
									$(".rateit").rateit();
									$.bootstrapSortable();
								});
							})
							.fail(function(e, errorThrown, jqXHR) {
								console.log(e);
								$("#output").html("Oops: <br>" + JSON.stringify(findreviewer_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
							});
						});

						$("#address").keyup(function() {
							$("#result").remove();
							var findreviewer_formdata = {
								"action" : "findreviewersubmit",
								"fields" : $("select[name=fields]").val(),
								"name"	 : $("input[name=name]").val(),
								"address": $("input[name=address]").val()
							};
							$.ajax({
								type        : "POST",
								url         : "backend.php",
								data        : findreviewer_formdata,
								dataType    : "json",
								encode      : true
							})
							.done(function(e) {
								$("#findreviewerdiv").append("<div id='result' class='added'>" + e.html + "</div>");
								$(document).ready( function() {
									$(".rateit").rateit();
									$.bootstrapSortable();
								});
							})
							.fail(function(e, errorThrown, jqXHR) {
								console.log(e);
								$("#output").html("Oops: <br>" + JSON.stringify(findreviewer_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
							});
						});

						$("#fields").change(function() {
							$("#result").remove();
							var findreviewer_formdata = {
								"action" : "findreviewersubmit",
								"fields" : $("select[name=fields]").val(),
								"name"	 : $("input[name=name]").val(),
								"address": $("input[name=address]").val()
							};
							$.ajax({
								type        : "POST",
								url         : "backend.php",
								data        : findreviewer_formdata,
								dataType    : "json",
								encode      : true
							})
							.done(function(e) {
								$("#findreviewerdiv").append("<div id='result' class='added'>" + e.html + "</div>");
								$(document).ready( function() {
									$(".rateit").rateit();
									$.bootstrapSortable();
								});
							})
							.fail(function(e, errorThrown, jqXHR) {
								console.log(e);
								$("#output").html("Oops: <br>" + JSON.stringify(findreviewer_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
							});
						});

						$(document).on("click", ".reviewer", function(e) {
							if(confirm("soll das Buch diesem Rezensenten zugeordnet werden?")){
								$(document).off("click", ".reviewer");
								var revid = $(this).data('reviewerid');
								var addreview_formdata = {
								"action" : "addreview",
								"bookid" : bookid,
								"revid"	 : revid
								};
								$.ajax({
									type        : "POST",
									url         : "backend.php",
									data        : addreview_formdata,
									dataType    : "json",
									encode      : true
								})
								.done(function(e) {
									$(".added").remove();
									$("#output").html(bookid + " wurde " + revid + " zugeordnet.");

								})
								.fail(function(e, errorThrown, jqXHR) {
									console.log(e);
									$("#output").html("Oops: beim zuordnen ist was schiefgelaufen<br><br>" + JSON.stringify(addreview_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
								});
							}
						});
					});
				})
				.fail(function( jqXHR, textStatus, errorThrown ) {
					console.log(e);
					$("#output").html("Oops: beim laden der rezensentensuche ist was schiefgelaufen<br><br>" + textStatus + "<br>___<br>" + errorThrown + "<br>___<br>" + jqXHR.responseText);
				});
			});
			$("#deletebook").off('click').click(function (e) {
				var bookdelete_formdata = {
					"action" 		: "bookdelete",
					"bookid"		: bookid
				};
				if(confirm("Wirklich entfernen?")){
					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : bookdelete_formdata,
						dataType    : "json",
						encode      : true
					})
					.done(function(e) {
						console.log(e);
						$("#bookmodal").modal("hide");
					})
					.fail(function(jqXHR, textStatus, errorThrown) {
						console.log(jqXHR.response);
						console.log(e.response);
						$("#output").html("Oops: Beim L&ouml;schen des Buches ist was schiefgelaufen <br>" +  jqXHR.responseText);
					});
				}
			});
		});
		$(document).on("hide.bs.modal", "#bookmodal", function (e) {
			var findbooks_formdata = {
				"action" : "booksadmin",
				"title"	 : $("input[name=titlesearch]").val(),
				"author" : $("input[name=authorsearch]").val()
			};
			$.ajax({
				type        : "POST",
				url         : "backend.php",
				data        : findbooks_formdata,
				dataType    : "json",
				encode      : true
			})
			.done(function(e) {
				$(".added").remove();
				$("#output").append(e.html);
				$(document).ready( function() {

				});
			})
			.fail(function(e, errorThrown, jqXHR) {
				console.log(e);
				$("#output").html("Oops: <br>" + JSON.stringify(findbooks_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
			});
		});
	});

	$(document).on("submit", "#fakebook", function (e) {
		e.preventDefault();
		var fakebook_formdata = {
			"action" : "fakebook",
			"value"	 : $('select[name=fakebooks]').val()
		};
		$.ajax({
			type        : "POST",
			url         : "backend.php",
			data        : fakebook_formdata,
			dataType    : "json",
			encode      : true
		})

		// using the done promise callback
		.done(function(e) {

		// log e to the console so we can see
			console.log(e);

		})
		.fail(function(e, errorThrown, jqXHR) {
			console.log(e);
			$("#output").html("Oops: <br>" + fakebook_formdata.value + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR));
		});

	});

//  Rezensenten

	$("#newreviewer").click(function() {
		$(".added").remove();
		$("#bc1").html("Neuer Rezensent");
		$(".breadcrumbx").addClass("hidden");
		$("#main").hide(1000);
		$.ajax({
				type        : "POST",
				url         : "backend.php",
				data        : {"action":"newreviewer"},
				dataType    : "json"
		})
		.done(function(e) {
			$("#output").html(e.html);
			$(document).ready( function() {
				$(".rateit").rateit();
				$("#fields").select2();
				var max_fields	= 5; //maximum input boxes allowed
				var nr_of_address = 1;
				var nr_of_email = 1;
				$(document).on("click","#addaddress", function(e) {
					e.preventDefault();
					if(nr_of_address < max_fields){
						nr_of_address++;
						$(".address-group:last").after("\
							<div id='address-group" + nr_of_address + "' class='form-group address-group'>\
								<label class='control-label col-sm-3' for='address" + nr_of_address + "'>" + nr_of_address + ". Adresse</label>\
								<div class='col-sm-5'>\
									<textarea rows='4' class='form-control address' name='address" + nr_of_address + "' id='address" + nr_of_address + "'></textarea>\
								</div>\
								<div class='col-sm-2'>\
									<a href='#' id='remaddress" + nr_of_address + "' class='remove_address'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>Entfernen</a>\
								</div><div class='col-sm-2 addresstype' data-type='other'></div>\
							</div>\
						");
					}
				});
				$(document).on("click",".remove_address", function(e){
					e.preventDefault();
					if(confirm("Wirklich entfernen?")){
						$(this).parent("div").parent("div").remove(); nr_of_address--;
					}
				});
				$(document).on("click","#addemail", function(e) {
					e.preventDefault();
					if(nr_of_email < max_fields){
						nr_of_email++;
						$(".email-group:last").after("\
							<div id='email-group" + nr_of_email + "' class='form-group email-group'>\
								<label class='control-label col-sm-3' for='email" + nr_of_email + "'>" + nr_of_email + ". E-Mail-Adresse</label>\
								<div class='col-sm-5'>\
									<input type='text' class='form-control email' name='email" + nr_of_email + "' id='email" + nr_of_email + "'>\
								</div>\
								<div class='col-sm-2'>\
									<a href='#' id='rememail" + nr_of_email + "' class='remove_email'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>Entfernen</a>\
								</div><div class='col-sm-2 emailtype' data-type='other'></div>\
							</div>\
						");
					}
				});
				$(document).on("click",".remove_email", function(e){
					e.preventDefault();
					if(confirm("Wirklich entfernen?")){
						$(this).parent("div").parent("div").remove(); nr_of_email--;
					}
				});
			});
		})
		.fail(function( jqXHR, textStatus, errorThrown ) {
			$("#output").html(textStatus + "<br>___<br>" + errorThrown + "<br>___<br>" + jqXHR.responseText);
		});
	});

	$(document).on("submit", "#newreviewerform", function (e) {
		e.preventDefault();
		var address = [];
			$(".address").each(function(){
				address.push($(this).val());
			});
			var addresstype = [];
			$(".addresstype").each(function(){
				if( ! $(this).hasClass("hidden"))
					addresstype.push($(this).attr("data-type"));
			});
			var email = [];
			$(".email").each(function(){
				email.push($(this).val());
			});
			var emailtype = [];
			$(".emailtype").each(function(){
				if( ! $(this).hasClass("hidden") )
					emailtype.push($(this).attr("data-type"));
			});
			var newreviewer_formdata = {
				"action" 		: "newreviewersubmit",
				"title"			: $("input[name=title]").val(),
				"firstname"		: $("input[name=firstname]").val(),
				"lastname"		: $("input[name=lastname]").val(),
				"gender"		: $("input[name=gender]").val(),
				"note"			: $("textarea[name=note]").val(),
				"published"		: $("input[name=published]").val(),
				"requests"		: $("input[name=requests]").val(),
				"denied"		: $("input[name=denied]").val(),
				"warned"		: $("input[name=warned]").val(),
				"published_in"	: $("input[name=published_in]").val(),
				"form"			: $("input[name=rate_form]").val()*10,
				"content"		: $("input[name=rate_content]").val()*10,
				"formnote"		: $("textarea[name=rateform_note]").val(),
				"contentnote"	: $("textarea[name=ratecontent_note]").val(),
				"fields"		: $("select[name=fields]").val(),
				"address"		: address,
				"addresstype"	: addresstype,
				"email"			: email,
				"emailtype"		: emailtype

			};

		if ( $( "#lastname" ).val().length == 0 ) {
			// Usually show some kind of error message here
			$("#lastname-group").after("<div class='help-block'>Bitte sicherstellen, dass der Nachname ausgef&uuml;llt ist</div>");
			console.log(newreviewer_formdata);
		} else {

			$.ajax({
				type        : "POST",
				url         : "backend.php",
				data        : newreviewer_formdata,
				dataType    : "json",
				encode      : true
			})

			// using the done promise callback
			.done(function(e) {

			// log e to the console so we can see
				console.log(e);
				$("#output").html("Erfolgreich gespeichert: <br>" + JSON.stringify(newreviewer_formdata) + "<br><br>" +  e.success + "<br><br>" +  e.html);
			})
			.fail(function(e, errorThrown, jqXHR) {
				console.log(addresstype);
				$("#output").html("Oops: <br>" + newreviewer_formdata.lastname + "<br><br>" + JSON.stringify(newreviewer_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR));
			});


		};
	});

	$(document).on("submit", "#fakereviewer", function (e) {
		e.preventDefault();
		var fakereviewer_data = {
			"action" : "fakereviewer",
			"value"	 : $('select[name=fakereviewers]').val()
		};
		$.ajax({
			type        : "POST",
			url         : "backend.php",
			data        : fakereviewer_data,
			dataType    : "json",
			encode      : true
		})

		// using the done promise callback
		.done(function(e) {

		// log e to the console so we can see
			console.log(e);

		})
		.fail(function(e, errorThrown, jqXHR) {
			console.log(e);
			$("#output").html("Oops: <br>" + fakereviewer_data.value + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR));
		});

	});

    $("#findreviewer").click(function() {
		$(".added").remove();
		$("#bc1").html("Rezensenten finden");
		$(".breadcrumbx").addClass("hidden");
		$("#main").hide(1000);
		$.ajax({
				type        : "POST",
				url         : "backend.php",
				data        : {"action":"findreviewer"},
				dataType    : "json"
		})
		.done(function(e) {
			$("#output").html(e.html);
			$(document).ready( function() {
				$("#fields").select2({
					placeholder: "...",
					allowClear: true
				});
				$("#output").after("<div id='findreviewerdiv' class='col-sm-10 col-sm-offset-1 added'></div>");

				$("#name").keyup(function() {
					$("#result").remove();
					var findreviewer_formdata = {
						"action" : "findreviewersubmit",
						"fields" : $("select[name=fields]").val(),
						"name"	 : $("input[name=name]").val(),
						"address": $("input[name=address]").val()
					};
					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : findreviewer_formdata,
						dataType    : "json",
						encode      : true
					})
					.done(function(e) {
						$("#findreviewerdiv").append("<div id='result' class='added'>" + e.html + "</div>");
						$(document).ready( function() {
							$(".rateit").rateit();
							$.bootstrapSortable();
						});
					})
					.fail(function(e, errorThrown, jqXHR) {
						console.log(e);
						$("#output").html("Oops: <br>" + JSON.stringify(findreviewer_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
					});
				});

				$("#address").keyup(function() {
					$("#result").remove();
					var findreviewer_formdata = {
						"action" : "findreviewersubmit",
						"fields" : $("select[name=fields]").val(),
						"name"	 : $("input[name=name]").val(),
						"address": $("input[name=address]").val()
					};
					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : findreviewer_formdata,
						dataType    : "json",
						encode      : true
					})
					.done(function(e) {
						$("#findreviewerdiv").append("<div id='result' class='added'>" + e.html + "</div>");
						$(document).ready( function() {
							$(".rateit").rateit();
							$.bootstrapSortable();
						});
					})
					.fail(function(e, errorThrown, jqXHR) {
						console.log(e);
						$("#output").html("Oops: <br>" + JSON.stringify(findreviewer_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
					});
				});

				$("#fields").change(function() {
					$("#result").remove();
					var findreviewer_formdata = {
						"action" : "findreviewersubmit",
						"fields" : $("select[name=fields]").val(),
						"name"	 : $("input[name=name]").val(),
						"address": $("input[name=address]").val()
					};
					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : findreviewer_formdata,
						dataType    : "json",
						encode      : true
					})
					.done(function(e) {
						$("#findreviewerdiv").append("<div id='result' class='added'>" + e.html + "</div>");
						$(document).ready( function() {
							$(".rateit").rateit();
							$.bootstrapSortable();
						});
					})
					.fail(function(e, errorThrown, jqXHR) {
						console.log(e);
						$("#output").html("Oops: <br>" + JSON.stringify(findreviewer_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
					});
				});
				$(document).on("click", ".reviewer", function(e) {
					alert("yay");
				});
			});
		})
		.fail(function(e, jqXHR, textStatus, errorThrown ) {
			console.log(e);
			$("#output").html(textStatus + "<br>___<br>" + errorThrown + "<br>___<br>" + jqXHR.responseText);
		});
	});

	$("#reviewers").click(function() {
		$(".added").remove();
		$("#bc1").html("Alle Rezensenten");
		$(".breadcrumbx").addClass("hidden");
		$("#main").hide(1000);
		$.ajax({
			type        : "POST",
			url         : "backend.php",
			data        : {"action":"reviewers"},
			dataType    : "json"
		})
		.done(function(e) {
			$("#output").html(e.html);
			$(document).ready( function() {
				$(".rateit").rateit();
			});
		})
		.fail(function( jqXHR, textStatus, errorThrown ) {
			$("#output").html(textStatus + "<br>___<br>" + errorThrown + "<br>___<br>" + jqXHR.responseText);
		});
	});

	$("#revadmin").click(function() {
		$("#output").empty();
		$(".added").remove();
		$("#main").hide(1000);
		$("#bc1").html("Rezensentenverwaltung");
		$(".breadcrumbx").addClass("hidden");
		$.ajax({
			type        : "POST",
			url         : "backend.php",
			data        : {
						"action":"revadmin",
						"fields" : "",
						"name"	 : "",
						"address": ""},
			dataType    : "json"
		})
		.done(function(e) {
			$("#output").html(e.html2 + e.html);
			$(document).ready( function() {
				$("#fieldssearch").select2({
					placeholder: "...",
					allowClear: true
				});
				$(".rateit").rateit();
				$.bootstrapSortable();
			});
		})
		.fail(function(e, jqXHR, textStatus, errorThrown ) {
			console.log(e);
			$("#output").html("Oops: beim laden der rezensentensuche ist was schiefgelaufen<br><br>" + textStatus + "<br>___<br>" + errorThrown + "<br>___<br>" + jqXHR.responseText);
		});
		$(document).on("keyup", "#namesearch", function() {
			$(".added").remove();
			var revadmin_formdata = {
				"action" : "revadmin",
				"fields" : $("select[name=fieldssearch]").val(),
				"name"	 : $("input[name=namesearch]").val(),
				"address": $("input[name=addresssearch]").val()
			};
			$.ajax({
				type        : "POST",
				url         : "backend.php",
				data        : revadmin_formdata,
				dataType    : "json",
				encode      : true
			})
			.done(function(e) {
				$("#output").append(e.html);
				$(document).ready( function() {
					$(".rateit").rateit();
					$.bootstrapSortable();
				});
			})
			.fail(function(e, errorThrown, jqXHR) {
				console.log(e);
				$("#output").html("Oops: <br>" + JSON.stringify(revadmin_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
			});
		});
		$(document).on("keyup", "#addresssearch", function() {
			$(".added").remove();
			var revadmin_formdata = {
				"action" : "revadmin",
				"fields" : $("select[name=fieldssearch]").val(),
				"name"	 : $("input[name=namesearch]").val(),
				"address": $("input[name=addresssearch]").val()
			};
			$.ajax({
				type        : "POST",
				url         : "backend.php",
				data        : revadmin_formdata,
				dataType    : "json",
				encode      : true
			})
			.done(function(e) {
				$("#output").append(e.html);
				$(document).ready( function() {
					$(".rateit").rateit();
					$.bootstrapSortable();
				});
			})
			.fail(function(e, errorThrown, jqXHR) {
				console.log(e);
				$("#output").html("Oops: <br>" + JSON.stringify(revadmin_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
			});
		});
		$(document).on("change", "#fieldssearch", function() {
			$(".added").remove();
			var revadmin_formdata = {
				"action" : "revadmin",
				"fields" : $("select[name=fieldssearch]").val(),
				"name"	 : $("input[name=namesearch]").val(),
				"address": $("input[name=addresssearch]").val()
			};
			$.ajax({
				type        : "POST",
				url         : "backend.php",
				data        : revadmin_formdata,
				dataType    : "json",
				encode      : true
			})
			.done(function(e) {
				$("#output").append(e.html);
				$(document).ready( function() {
					$(".rateit").rateit();
					$.bootstrapSortable();
				});
			})
			.fail(function(e, errorThrown, jqXHR) {
				console.log(e);
				$("#output").html("Oops: <br>" + JSON.stringify(revadmin_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
			});
		});
		var nr_of_address = 0;
		var nr_of_email = 0;
		$(document).on("click", ".reviewer", function(e) {
			var revid = $(this).data('reviewerid');
			var modal = $("#revmodal");
			$.ajax({
				type        : "POST",
				url         : "backend.php",
				data        : {"action":"revdata", "revid":revid},
				dataType    : "json",
				encode      : true
			})
			.done(function(e) {
				$(document).ready( function() {
					var revfirstname    = e.firstname;
					var revlastname     = e.lastname;
					var revtitle        = e.title;
					var revgender       = e.gender;
					var revpublished    = e.published;
					var revrequests     = e.requests;
					var revdenied       = e.denied;
					var revwarned       = e.warned;
					var revnote         = e.note;
					var revpublished_in = e.published_in;
					var revform         = e.form / 10;
					var revcontent      = e.content / 10;
					var revformnote     = e.formnote ;
					var revcontentnote  = e.contentnote;
					var revaddress		= e.address;
					var revemail		= e.email;
					var revfields		= e.fields;
					modal.find("#firstname").val(revfirstname);
					modal.find("#lastname").val(revlastname);
					modal.find("#title").val(revtitle);
					modal.find("label").removeClass("active");
					modal.find("label[for=" + revgender + "]").addClass("active");
					modal.find("#published").val(revpublished);
					modal.find("#requests").val(revrequests);
					modal.find("#denied").val(revdenied);
					modal.find("#warned").val(revwarned);
					modal.find("#note").val(revnote);
					modal.find("#published_in").val(revpublished_in);
					modal.find("#rate_form_div").rateit("value", revform);
					modal.find("#rateform_note").val(revformnote);
					modal.find("#rate_content_div").rateit("value", revcontent);
					modal.find("#ratecontent_note").val(revcontentnote);
					var revfieldsarray = revfields.split(",");
					modal.find("#fields").val(revfieldsarray);
					modal.find("#fields").select2();
					$.each(revaddress, function(i, val){
						$(".address-group:last").after("\
							<div id='address-group" + i + "' class='added form-group address-group'>\
								<label class='control-label col-sm-3' for='address" + i + "'>" + (val[1]=="other"?"Weitere A":"Standarda") + "dresse</label>\
								<div class='col-sm-5'>\
									<textarea rows='4' class='form-control address' name='address" + i + "' id='address" + i + "'>" + val[0] + "</textarea>\
								</div>\
								<div class='col-sm-2'>\
									<a href='#' id='remaddress" + i + "' class='remove_address'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>Entfernen</a>\
								</div><div class='col-sm-2 addresstype' data-type='other'></div>\
							</div>\
						");
					});
					$.each(revemail, function(i, val){
						$(".email-group:last").after("\
							<div id='email-group" + i + "' class='added form-group email-group'>\
								<label class='control-label col-sm-3' for='email" + i + "'>" + (val[1]=="other"?"Weitere ":"Standard-") + "E-Mail-Adresse</label>\
								<div class='col-sm-5'>\
									<input type='text' class='form-control email' name='email" + i + "' id='email" + i + "' value='" + val[0] + "'>\
								</div>\
								<div class='col-sm-2'>\
									<a href='#' id='rememail" + i + "' class='remove_email'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>Entfernen</a>\
								</div><div class='col-sm-2 emailtype' data-type='other'></div>\
							</div>\
						");
					});
					nr_of_email = revemail.length + 1;
					nr_of_address = revaddress.length + 1;
				console.log(e);
				});
			})
			.fail(function(e, errorThrown, jqXHR) {
				console.log(e);
				$("#output").html("Oops: <br>" + JSON.stringify(e) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
			});
			$("#revmodal").on("shown.bs.modal", function () {
				$("#firstname").focus()
			})
			/*
			$("#updatebook").off("click").click(function (e) {
				var bookupdate_formdata = {
					"action" 		: "bookupdate",
					"bookid"		: bookid,
					"title"			: $('input[name=title]').val(),
					"author"		: $('input[name=author]').val(),
					"publisher"		: $('input[name=publisher]').val(),
					"pub_country"	: $('input[name=pub_country]').val(),
					"price"			: $('input[name=price]').val(),
					"note"			: $('textarea[name=note]').val(),
					"pages"			: $('input[name=pages]').val(),
					"series"		: $('input[name=series]').val(),
					"nr_in_series"	: $('input[name=nr_in_series]').val(),
					"pub_date"		: $('input[name=pub_date]').val(),
					"isbn"			: $('input[name=isbn]').val()
				};
				$.ajax({
					type        : "POST",
					url         : "backend.php",
					data        : bookupdate_formdata,
					dataType    : "json",
					encode      : true
				})
				.done(function(e) {
					console.log(e);
					$("#bookmodal").modal("hide");
				})
				.fail(function(e, errorThrown, jqXHR) {
					console.log(e);
					$("#output").html("Oops: Beim speichern des buches ist was schiefgelaufen<br><br><br>" + bookupdate_formdata.title + "<br><br>" + e.ans + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR));
				});
			});
			$("#findreviewermodal").off("click").click(function (e) {
				$("#output").html(" id:" + bookid + "<br>");

			});*/
			$(document).on("click","#addaddress", function(e) {
				e.preventDefault();
				if(nr_of_address < 5){
					nr_of_address++;
					$(".address-group:last").after("\
						<div id='address-group" + nr_of_address + "' class='form-group address-group'>\
							<label class='control-label col-sm-3' for='address" + nr_of_address + "'>" + nr_of_address + ". Adresse</label>\
							<div class='col-sm-5'>\
								<textarea rows='4' class='form-control address' name='address" + nr_of_address + "' id='address" + nr_of_address + "'></textarea>\
							</div>\
							<div class='col-sm-2'>\
								<a href='#' id='remaddress" + nr_of_address + "' class='remove_address'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>Entfernen</a>\
							</div><div class='col-sm-2 addresstype' data-type='other'></div>\
						</div>\
					");
				}
			});
			$(document).on("click",".remove_address", function(e){
				e.preventDefault();
				if(confirm("Wirklich entfernen?")){
					$(this).parent("div").parent("div").remove(); nr_of_address--;
				}
			});
			$(document).on("click","#addemail", function(e) {
				e.preventDefault();
				if(nr_of_email < 5){
					nr_of_email++;
					$(".email-group:last").after("\
						<div id='email-group" + nr_of_email + "' class='form-group email-group'>\
							<label class='control-label col-sm-3' for='email" + nr_of_email + "'>" + nr_of_email + ". E-Mail-Adresse</label>\
							<div class='col-sm-5'>\
								<input type='text' class='form-control email' name='email" + nr_of_email + "' id='email" + nr_of_email + "'>\
							</div>\
							<div class='col-sm-2'>\
								<a href='#' id='rememail" + nr_of_email + "' class='remove_email'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>Entfernen</a>\
							</div><div class='col-sm-2 emailtype' data-type='other'></div>\
						</div>\
					");
				}
			});
			$(document).on("click",".remove_email", function(e){
				e.preventDefault();
				if(confirm("Wirklich entfernen?")){
					$(this).parent("div").parent("div").remove(); nr_of_email--;
				}
			});
			$("#deleterev").off("click").click(function (e) {
				var revdelete_formdata = {
					"action" : "revdelete",
					"revid"	 : revid
				};
				if(confirm("Wirklich entfernen?")){
					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : revdelete_formdata,
						dataType    : "json",
						encode      : true
					})
					.done(function(e) {
						console.log(e);
						$("#revmodal").modal("hide");
					})
					.fail(function(e, errorThrown, jqXHR) {
						console.log(e);
						$("#output").html("Oops: Beim l&ouml;schen des rezensenten ist was schiefgelaufen<br>" +  errorThrown + "<br><br>" +  JSON.stringify(e));
					});
				}
			});
		});
		$(document).on("hide.bs.modal", "#revmodal", function (e) {
			var revadmin_formdata = {
				"action" : "revadmin",
				"fields" : $("select[name=fieldssearch]").val(),
				"name"	 : $("input[name=namesearch]").val(),
				"address": $("input[name=addresssearch]").val()
			};
			$.ajax({
				type        : "POST",
				url         : "backend.php",
				data        : revadmin_formdata,
				dataType    : "json",
				encode      : true
			})
			.done(function(e) {
				$(".added").remove();
				$("#output").append(e.html);
				$(document).ready( function() {
					$(".rateit").rateit();
					$.bootstrapSortable();
				});
			})
			.fail(function(e, errorThrown, jqXHR) {
				console.log(e);
				$("#output").html("Oops: <br>" + JSON.stringify(revadmin_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
			});
		});
	});
	$("#newreview").click(function() {
		$(".added").remove();
		$("#bc1").html("Neue Rezension");
		$(".breadcrumbx").addClass("hidden");
		$("#main").hide(1000);
		$.ajax({
				type        : "POST",
				url         : "backend.php",
				data        : {"action":"newreview"},
				dataType    : "json"
			})
			.done(function(e) {
				$("#output").html(e.html);
				$(document).ready( function() {

				});
			})
			.fail(function( jqXHR, textStatus, errorThrown ) {
				$("#output").html(textStatus + "<br>___<br>" + errorThrown + "<br>___<br>" + jqXHR.responseText);
			});
	});

	$("#reviews").click(function() {
		$(".added").remove();
		$("#bc1").html("Alle Rezensionen");
		$(".breadcrumbx").addClass("hidden");
		$("#main").hide(1000);
		$.ajax({
			type        : "POST",
			url         : "backend.php",
			data        : {"action":"reviews", "name":"", "book":""},
			dataType    : "json"
		})
		.done(function(e) {
			$("#output").html(e.html2 + e.html);
			$("#rev_deadline_date").datepicker().datepicker("option", $.datepicker.regional.de).datepicker("setDate", "+6m").datepicker("option", "changeYear", "true");
			$("#review_arrived_date").datepicker().datepicker("option", $.datepicker.regional.de).datepicker("setDate", "today").datepicker("option", "changeYear", "true");
			$(".cb_container").click(function () {
			   var $checkbox = $(this).find('input');
			   $checkbox.prop('checked', !$checkbox[0].checked);
			});
			/*$("#rev_decline_btn")
			$("#rev_deadline_btn")
			$("#rev_warning_btn")
			$("#rev_change_btn")
			$("#rev_zdl_btn")*/
			$(document).ready( function() {
				$(".rateit").rateit();
				$(".reviewsearch").keyup(function() {
					var findreview_formdata = {
						"action" : "reviews",
						"name"	 : $("input[name=name]").val(),
						"book"	 : $("input[name=book]").val()
					};
					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : findreview_formdata,
						dataType    : "json",
						encode      : true
					})
					.done(function(e) {
						$(".added").remove();
						$("#output").append(e.html);

					})
					.fail(function(e, errorThrown, jqXHR) {
						console.log(e);
						$("#output").html("Oops: <br>" + JSON.stringify(findreview_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
					});
				});
				$(document).on("click", ".event", function(e) {
					var reviewid = $(this).data("reviewid");
				});
			});
		})
		.fail(function( jqXHR, textStatus, errorThrown ) {
			$("#output").html(textStatus + "<br>___<br>" + errorThrown + "<br>___<br>" + jqXHR.responseText);
		});
	});

	$("#calleditor").click(function(){
		$(".added").remove();
		$("#foop").html();
	});
	$("#bvbc").click(function(){
		$("body").css("background-image", "url(http://upload.wikimedia.org/wikipedia/commons/6/67/Borussia_Dortmund_logo.svg)");
	});
	$("#fcbc").click(function(){
		$("body").css("background-image", "url(http://upload.wikimedia.org/wikipedia/commons/c/c5/Logo_FC_Bayern_M%C3%BCnchen.svg)");
	});
	$("#genug").click(function(){
		$("body").css("background-image", "none");
	});
});
