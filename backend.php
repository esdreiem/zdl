<?php
//	header("Content-Type: application/json");
	$dbconn = pg_connect("host=localhost port=5432 dbname=ZDL2 user=postgres connect_timeout=5");
	$out = array();
	
	if($_POST["action"] == "newbook"){
		$out["html"] = "
			<form name='newbookform' id='newbookform' class='form-horizontal' role='form'>
			
			<!-- title -->
			<div id='title-group' class='form-group'>
				<label class='control-label col-sm-2' for='title'>Titel</label>
				<div class='col-sm-9'>
					<input type='text' class='form-control' name='title' id='title'>
				</div>
				<div class='col-sm-1'>
					<button onClick='addText(\"title\",\"&#8212;\");' type='button'>&#8212;</button>
				</div>
				<!-- errors will go here -->
			</div>
			
			<!-- author -->
			<div id='author-group' class='form-group'>
				<label class='control-label col-sm-2' for='author'>Autor</label>
				<div class='col-sm-10'>
					<input type='text' class='form-control' name='author' id='author'>
				</div>
				<!-- errors will go here -->
			</div>
			
			<!-- publisher -->
			<div id='publisher-group' class='form-group'>
				<label class='control-label col-sm-2' for='publisher'>Verlag</label>
				<div class='col-sm-10'>
					<input type='text' class='form-control' name='publisher' id='publisher'>
				</div>
				<!-- errors will go here -->
			</div>
			
			<!-- pub_country -->
			<div id='pub_country-group' class='form-group'>
				<label class='control-label col-sm-2' for='pub_country'>Verlagsort</label>
				<div class='col-sm-10'>
					<input type='text' class='form-control' name='pub_country' id='pub_country'>
				</div>
				<!-- errors will go here -->
			</div>
			
			<!-- price -->
			<div id='price-group' class='form-group'>
				<label class='control-label col-sm-2' for='price'>Preis</label>
				<div class='col-sm-9'>
					<input type='text' class='form-control' name='price' id='price'>
				</div>
				<div class='col-sm-1'>
					<button onClick='addText(\"price\",\"&pound;\");' type='button'><span class='glyphicon glyphicon-gbp'></span></button>
				</div>
				<!-- errors will go here -->
			</div>
			
			<!-- note -->
			<div id='note-group' class='form-group'>
				<label class='control-label col-sm-2' for='note'>Bemerkungen</label>
				<div class='col-sm-10'>
					<textarea class='form-control' rows='4' name='note' id='note'></textarea>
				</div>
				<!-- errors will go here -->
			</div>
			
			<!-- pages -->
			<div id='pages-group' class='form-group'>
				<label class='control-label col-sm-2' for='pages'>Seiten</label>
				<div class='col-sm-9'>
					<div class='input-group'>
						<input type='text' class='form-control' name='pages' id='pages'>
						<span class='input-group-addon'>&nbsp;S.</span>
					</div>
				</div>
				<div class='col-sm-1'>
					<button onClick='addText(\"pages\",\"&#8212;\");' type='button'>&#8212;</button>
				</div>

				<!-- errors will go here -->
			</div>
			
			<!-- series -->
			<div id='series-group' class='form-group'>
				<label class='control-label col-sm-2' for='series'>Reihe</label>
				<div class='col-sm-9'>
					<input type='text' class='form-control' name='series' id='series'>
				</div>
				<div class='col-sm-1'>
					<button onclick='addText(\"series\",\"&#8212;\");' type='button'>&#8212;</button>
				</div> 
				<!-- errors will go here -->
			</div>
			
			<!-- nr_in_series -->
			<div id='nr_in_series-group' class='form-group'>
				<label class='control-label col-sm-2' for='nr_in_series'>Nummer in Reihe</label>
				<div class='col-sm-4'>
					<input type='text' class='form-control' name='nr_in_series' id='nr_in_series'>
				</div>
				<div class='col-sm-6'></div>
				<!-- errors will go here -->
			</div>
			
			<!-- pub_date -->
			<div id='pub_date-group' class='form-group'>
				<label class='control-label col-sm-3' for='pub_date'>Publikationsjahr</label>
				<div class='col-sm-3'>
					<input type='text' class='form-control' name='pub_date' id='pub_date'>
				</div>
				<div class='col-sm-6'></div>
				<!-- errors will go here -->
			</div>
			
			<!-- isbn -->
			<div id='isbn-group' class='form-group'>
				<label class='control-label col-sm-2' for='isbn'>ISBN</label>
				<div class='col-sm-4'>
					<input type='text' class='form-control' name='isbn' id='isbn'>
				</div>
				<div class='col-sm-6'></div>
				<!-- errors will go here -->
			</div>

			<button id='savecontbook' name='savecontbook' class='btn btn-success'>Speichern und Rezensenten suchen</button>
			<button id='savebook' name='savebook' class='btn btn-success'>Speichern <span class='glyphicon glyphicon-floppy-disk'></span></button>
			</form>
			
			<form name='fakebook' id='fakebook' class='form-inline'>
			  <div class='form-group'>
				<label for='fakebooks'>wieviele Fakeb&uuml;cher?</label>
				<select class='form-control' id='fakebooks' name='fakebooks'>
				  <option value='1'>1</option>
				  <option value='5'>5</option>
				  <option value='10'>10</option>
				  <option value='20'>20</option>
				  <option value='50'>50</option>
				</select>
			  </div>
			  <button type='submit' class='btn btn-default'>Go!</button>
			</form>
			";
		echo json_encode($out);
			
	}elseif($_POST["action"] == "titlesearch"){
		$q = $_POST["value"];
		$out["html"] = "<table class='table table-striped table-hover'>
				<tr class='info'>
					<th>Buch:</th>
					<th>Autor:</th>
				</tr>";
		$search = pg_query("select title, author from books where title ilike '%" . $q . "%'");
		while ($b=pg_fetch_row($search)) {
			$out["html"] .= 
				"<tr>
					<td> " . $b[0] . " </td>
					<td> " . $b[1] . " </td>
				</tr>";
		}
		$out["html"] .= "</table>";
		echo json_encode($out);

	}elseif($_POST["action"] == "authorsearch"){
		$q = $_POST["value"];
		$out["html"] = "<table class='table table-striped table-hover'>
				<tr class='info'>
					<th>Autor:</th>
					<th>Buch:</th>
				</tr>";
		$search = pg_query("select title, author from books where author ilike '%" . $q . "%'");
		while ($b=pg_fetch_row($search)) {
			$out["html"] .= 
				"<tr>
					<td> " . $b[1] . " </td>
					<td> " . $b[0] . " </td>
				</tr>";
		}
		$out["html"] .= "</table>";
		echo json_encode($out);		
		
	}elseif($_POST["action"] == "newbooksubmit"){
		$errors         = array();      // array to hold validation errors
		$out           = array();      // array to pass back data

		// validate the variables ======================================================
			// if any of these variables don"t exist, add an error to our $errors array

		if (empty($_POST["title"])) {
			$errors["title"] = "Der Titel fehlt";
		}

    // if there are any errors in our errors array, return a success boolean of false
		if ( ! empty($errors)) {
			// if there are items in our errors array, return those errors
			$out["success"] = false;
			$out["errors"]  = $errors;
			
		} else {

			// if there are no errors process our form, then return a message

			// DO ALL YOUR FORM PROCESSING HERE
			// THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)
			$newbooksubmit = pg_query("insert into books (
				title, author, publisher, pub_country, price, note, 
				pages, series, nr_in_series, pub_date, isbn) 
				values ('" .  $_POST["title"] . "', '" . $_POST["author"] . "', '" . 
				$_POST["publisher"] . "', '" . $_POST["pub_country"] . "', '" . 
				$_POST["price"] . "', '" . $_POST["note"] . "', '" . $_POST["pages"] . "', '" . 
				$_POST["series"] . "', '" . $_POST["nr_in_series"] . "', '" . $_POST["pub_date"] . "', '" . 
				$_POST["isbn"] . "') returning id");
			// show a message of success and provide a true success variable
			$out["id"] = pg_fetch_all($newbooksubmit)[0]["id"];
			$out["success"] = true;
			$out["message"] = "Success!";
		}
		echo json_encode($out);
			
	}elseif($_POST["action"] == "fakebook"){
		require_once "lib/Faker-master/src/autoload.php";
		// $faker = Faker\Factory::create('de_DE');
		$faker = Faker\Factory::create();
		$i = 0;
		while ($i < $_POST["value"])	{
			$san_title 			=	pg_escape_string(utf8_encode($faker->unique()->catchPhrase));
			$san_author 		=	pg_escape_string(utf8_encode($faker->name));
			$san_publisher		=	pg_escape_string(utf8_encode($faker->company));
			$san_pub_country	=	pg_escape_string(utf8_encode($faker->country));
			$san_price			=	pg_escape_string(utf8_encode($faker->randomFloat($nbDecimals = 2, $min = 0, $max = 100)));
			$san_note			=	pg_escape_string(utf8_encode($faker->text(50)));
			$san_series			=	pg_escape_string(utf8_encode($faker->catchPhrase));
			
			$temp = "insert into books (
			title, author, publisher, pub_country, price, note, 
			pages, series, nr_in_series, pub_date, isbn) 
			values ('" . $san_title . "', '" . $san_author . "', '" . 
			$san_publisher . "', '" . $san_pub_country . "', '" . 
			$san_price . "', " . ($faker->boolean(65) ? "'" . $san_note . "', " : "NULL, ")  . "'" . $faker->numberBetween($min = 50, $max = 1000) . "', '" . 
			$san_series . "', '" . $faker->randomDigit . "', '" . $faker->date($format = 'd.m.Y', $max = 'now') . "', '" . 
			$faker->ean13 . "')";
		
			$result = pg_query($temp);
			$i++;
		}
		$out["html"] = "all seems good";
		echo json_encode($out);
		
	}elseif($_POST["action"] == "books"){
		$out["html"] = "Noch nicht vergeben:<br>
			<table class='table table-striped table-hover'>
				<tr class='info'>
					<th>Buch:</th>
					<th>Autor:</th>
					<th>Bemerkungen:</th> 		
				</tr>";
		$all_books = pg_query("select title, author, note from books");
		$unassigned_books = pg_query("select books.id, title, author, note from books LEFT OUTER JOIN reviews on books.id = reviews.bookid where reviews.id is null");
		$halfassigned_books = pg_query("select books.id, title, author, note from books inner JOIN reviews on books.id = reviews.bookid where reviews.revid is null");
		$assigned_books = pg_query("select title, author, note from books inner join reviews on books.id = reviews.bookid");
		while ($b=pg_fetch_row($unassigned_books)) {
			$out["html"] .= "<tr class='book' data-bookid='" . $b[0] . "'>
					<td> " . $b[1] . " </td>
					<td> " . $b[2] . " </td>
					<td> " . $b[3] . " </td>
				</tr>";
		}
		
		$out["html"] .= "</table><br><br>Bereits vergeben:<br>
			<table class='table table-striped table-hover'>
				<tr class='info'>
					<th>Buch:</th>
					<th>Autor:</th>
					<th>Bemerkungen:</th> 		
				</tr>";
		while ($b=pg_fetch_row($assigned_books)) {
			$out["html"] .= "<tr>
					<td> " . $b[0] . " </td>
					<td> " . $b[1] . " </td>
					<td> " . $b[2] . " </td>
				</tr>";
		}
		$out["html"] .= "</table>";
		echo json_encode($out);

	}elseif($_POST["action"] == "booksadmin"){
		$out["html2"] = "<div class='modal fade' id='bookmodal' tabindex='-1' role='dialog' aria-labelledby='bookmodallabel' aria-hidden='true'>
			<div class='modal-dialog'>
				<div class='modal-content'>
					<div class='modal-header'>
						<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
						<h4 class='modal-title' id='bookmodallabel'>Daten &auml;ndern</h4>
					</div>
					<div class='modal-body'>
						<form class='form-horizontal' role='form'>
			
			<!-- title -->
			<div id='title-group' class='form-group'>
				<label class='control-label col-sm-2' for='title'>Titel</label>
				<div class='col-sm-9'>
					<input type='text' class='form-control' name='title' id='title'>
				</div>
				<div class='col-sm-1'>
					<button onClick='addText(\"title\",\"&#8212;\");' type='button'>&#8212;</button>
				</div>
				<!-- errors will go here -->
			</div>
			
			<!-- author -->
			<div id='author-group' class='form-group'>
				<label class='control-label col-sm-2' for='author'>Autor</label>
				<div class='col-sm-10'>
					<input type='text' class='form-control' name='author' id='author'>
				</div>
				<!-- errors will go here -->
			</div>
			
			<!-- publisher -->
			<div id='publisher-group' class='form-group'>
				<label class='control-label col-sm-2' for='publisher'>Verlag</label>
				<div class='col-sm-10'>
					<input type='text' class='form-control' name='publisher' id='publisher'>
				</div>
				<!-- errors will go here -->
			</div>
			
			<!-- pub_country -->
			<div id='pub_country-group' class='form-group'>
				<label class='control-label col-sm-2' for='pub_country'>Verlagsort</label>
				<div class='col-sm-10'>
					<input type='text' class='form-control' name='pub_country' id='pub_country'>
				</div>
				<!-- errors will go here -->
			</div>
			
			<!-- price -->
			<div id='price-group' class='form-group'>
				<label class='control-label col-sm-2' for='price'>Preis</label>
				<div class='col-sm-9'>
					<input type='text' class='form-control' name='price' id='price'>
				</div>
				<div class='col-sm-1'>
					<button onClick='addText(\"price\",\"&pound;\");' type='button'><span class='glyphicon glyphicon-gbp'></span></button>
				</div>
				<!-- errors will go here -->
			</div>
			
			<!-- note -->
			<div id='note-group' class='form-group'>
				<label class='control-label col-sm-2' for='note'>Bemerkungen</label>
				<div class='col-sm-10'>
					<textarea class='form-control' rows='4' name='note' id='note'></textarea>
				</div>
				<!-- errors will go here -->
			</div>
			
			<!-- pages -->
			<div id='pages-group' class='form-group'>
				<label class='control-label col-sm-2' for='pages'>Seiten</label>
				<div class='col-sm-9'>
					<div class='input-group'>
						<input type='text' class='form-control' name='pages' id='pages'>
						<span class='input-group-addon'>&nbsp;S.</span>
					</div>
				</div>
				<div class='col-sm-1'>
					<button onClick='addText(\"pages\",\"&#8212;\");' type='button'>&#8212;</button>
				</div>

				<!-- errors will go here -->
			</div>
			
			<!-- series -->
			<div id='series-group' class='form-group'>
				<label class='control-label col-sm-2' for='series'>Reihe</label>
				<div class='col-sm-9'>
					<input type='text' class='form-control' name='series' id='series'>
				</div>
				<div class='col-sm-1'>
					<button onclick='addText(\"series\",\"&#8212;\");' type='button'>&#8212;</button>
				</div> 
				<!-- errors will go here -->
			</div>
			
			<!-- nr_in_series -->
			<div id='nr_in_series-group' class='form-group'>
				<label class='control-label col-sm-2' for='nr_in_series'>Nummer in Reihe</label>
				<div class='col-sm-4'>
					<input type='text' class='form-control' name='nr_in_series' id='nr_in_series'>
				</div>
				<div class='col-sm-6'></div>
				<!-- errors will go here -->
			</div>
			
			<!-- pub_date -->
			<div id='pub_date-group' class='form-group'>
				<label class='control-label col-sm-3' for='pub_date'>Publikationsjahr</label>
				<div class='col-sm-3'>
					<input type='text' class='form-control' name='pub_date' id='pub_date'>
				</div>
				<div class='col-sm-6'></div>
				<!-- errors will go here -->
			</div>
			
			<!-- isbn -->
			<div id='isbn-group' class='form-group'>
				<label class='control-label col-sm-2' for='isbn'>ISBN</label>
				<div class='col-sm-4'>
					<input type='text' class='form-control' name='isbn' id='isbn'>
				</div>
				<div class='col-sm-6'></div>
				<!-- errors will go here -->
			</div>
				</form>
					</div>
					<div class='modal-footer'>
						<button type='button' id='deletebook' name='deletebook' class='btn btn-danger pull-left'><span class='glyphicon glyphicon-trash'></span>L&ouml;schen</button>
						<button type='button' id='findreviewermodal' name='findreviewermodal' class='btn btn-primary'>Rezensenten suchen</button>
						<button type='button' id='updatebook' name='updatebook' class='btn btn-success'>Speichern <span class='glyphicon glyphicon-floppy-disk'></span></button>
					</div>
				</div>
			</div>
		</div>
			<form>
			<!-- title -->
			<div class='form-group'>
				<label class='control-label col-sm-1' for='titlesearch'>Titel</label>
				<div class='col-sm-4'>
					<input type='text' class='booksearch form-control' name='titlesearch' id='titlesearch'>
				</div>
				<label class='control-label col-sm-1' for='authorsearch'>Autor</label>
				<div class='col-sm-4'>
					<input type='text' class='booksearch form-control' name='authorsearch' id='authorsearch'>
				</div>
				<div class='col-sm-1'>
					<button type='button' id='newbook_button' name='newbook_button' class='btn btn-success' data-toggle='modal' data-target='#bookmodal' data-bookid='0'>Neues Buch</button>
				</div>
			</div>
			</form>";
		$out["html"] = "
			<table class='added sortable table table-striped table-hover'>
				<thead><tr class='info'>
					<th>Titel:</th>
					<th>Autor:</th>
					<th>Bemerkungen:</th>
					<th></th>
				</tr></thead><tbody>";
		$title = $_POST["title"];
		$author = $_POST["author"];
		$unassigned_books = pg_query("select books.id, title, author, note from books LEFT OUTER JOIN reviews on books.id = reviews.bookid where reviews.id is null and books.title ilike '%" . $title . "%' and books.author ilike '%" . $author . "%'");
		$halfassigned_books = pg_query("select books.id, title, author, note from books inner JOIN reviews on books.id = reviews.bookid where reviews.revid is null and books.title ilike '%" . $title . "%' and books.author ilike '%" . $author . "%'");		
		$assigned_books = pg_query("select books.id, title, author, note from books inner join reviews on books.id = reviews.bookid where reviews.revid is not null and books.title ilike '%" . $title . "%' and books.author ilike '%" . $author . "%'");
		while ($b=pg_fetch_row($unassigned_books)) {
			$out["html"] .= "<tr class='book danger' data-toggle='modal' data-target='#bookmodal' data-bookid='" . $b[0] . "'>
					<td> " . $b[1] . " </td>
					<td> " . $b[2] . " </td>
					<td> " . $b[3] . " </td>
					<td data-value='0'></td>
				</tr>";
		}
		while ($b=pg_fetch_row($halfassigned_books)) {
			$out["html"] .= "<tr class='book warning' data-toggle='modal' data-target='#bookmodal' data-bookid='" . $b[0] . "'>
					<td> " . $b[1] . " </td>
					<td> " . $b[2] . " </td>
					<td> " . $b[3] . " </td>
					<td data-value='1'></td>
				</tr>";
		}
		while ($b=pg_fetch_row($assigned_books)) {
			$out["html"] .= "<tr class='book success' data-toggle='modal' data-target='#bookmodal' data-bookid='" . $b[0] . "'>
					<td> " . $b[1] . " </td>
					<td> " . $b[2] . " </td>
					<td> " . $b[3] . " </td>
					<td data-value='2'></td>
				</tr>";
		}
		$out["html"] .= "</tbody></table>";
		echo json_encode($out);
		
	}elseif($_POST["action"] == "bookdata"){
		$result = pg_query("select * from books where id = " . $_POST["bookid"]);
		while ($b=pg_fetch_row($result)){
			$out["id"] = $b[0];
			$out["title"] = $b[1];
			$out["author"] = $b[2];
			$out["publisher"] = $b[3];
			$out["pub_country"] = $b[4];
			$out["price"] = $b[5];
			$out["note"] = $b[6];
			$out["pages"] = $b[7];
			$out["series"] = $b[8];
			$out["nr_in_series"] = $b[9];
			$out["pub_date"] = $b[10];
			$out["isbn"] = $b[11];
		}
		echo json_encode($out);

	}elseif($_POST["action"] == "bookupdate"){
		$title = pg_escape_string(utf8_encode($_POST["title"]));
		$author = pg_escape_string(utf8_encode($_POST["author"]));
		$publisher = pg_escape_string(utf8_encode($_POST["publisher"]));
		$pub_country = pg_escape_string(utf8_encode($_POST["pub_country"]));
		$price = pg_escape_string(utf8_encode($_POST["price"]));
		$note = pg_escape_string(utf8_encode($_POST["note"]));
		$pages = pg_escape_string(utf8_encode($_POST["pages"]));
		$series = pg_escape_string(utf8_encode($_POST["series"]));
		$nr_in_series = pg_escape_string(utf8_encode($_POST["nr_in_series"]));
		$pub_date = pg_escape_string(utf8_encode($_POST["pub_date"]));
		$isbn = pg_escape_string(utf8_encode($_POST["isbn"]));

		$result = pg_query("update books set (title, author, publisher, pub_country, price, note, 
				pages, series, nr_in_series, pub_date, isbn) = ('" .  $title . "', '" . $author . "', '" . 
				$publisher . "', '" . $pub_country . "', '" . 
				$price . "', '" . $note . "', '" . $pages . "', '" . 
				$series . "', '" . $nr_in_series . "', '" . $pub_date . "', '" . 
				$isbn . "') where id = " . $_POST["bookid"]);
		$out["response"] = pg_last_error();
		echo json_encode($out);
		
	}elseif($_POST["action"] == "bookdelete"){
		$result = pg_query("delete from books where id = " . $_POST["bookid"]);
		//$out["response"] = pg_last_error();
		echo json_encode($out);
		
	}elseif($_POST["action"] == "newreviewer"){
		$out["html"] = "
			<form id='newreviewerform' name='newreviewerform' action='backend.php' method='POST' class='form-horizontal' role='form'>	
			
				<!-- title -->
				<div id='title-group' class='form-group'>
					<label class='control-label col-sm-3' for='title'>Titel</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='title' id='title'>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- firstname -->
				<div id='firstname-group' class='form-group'>
					<label class='control-label col-sm-3' for='firstname'>Vorname</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='firstname' id='firstname'>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- lastname -->
				<div id='lastname-group' class='form-group'>
					<label class='control-label col-sm-3' for='lastname'>Nachname</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='lastname' id='lastname'>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- gender -->
				<div id='gender-group' class='form-group'>
					<label class='control-label col-sm-3' for='gender'>Geschlecht</label>
					<div class='col-sm-9'>
						<div class='btn-group' data-toggle='buttons'>
							<label for='female' class='btn btn-default'>
								<input type='radio' value='female' name='gender' id='female' autocomplete='off'>
								Weiblich
							</label>
							<label for='male' class='btn btn-default'>
								<input type='radio' value='male' name='gender' id='male' autocomplete='off'>
								M&auml;nnlich
							</label>
							<label class='btn btn-default'>
								<input type='radio' value='other' name='gender' id='other' autocomplete='off'> Anderes
							</label>
						</div>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- address -->
				<div id='address-group1' class='form-group address-group'>
					<label class='control-label col-sm-3' for='address1'>Adresse (Standard)</label>
					<div class='col-sm-5'>
						<textarea rows='4' class='form-control address' name='address1' id='address1'></textarea>
					</div>
					<div class='col-sm-2 addresstype' data-type='standard'></div>
					<div class='col-sm-2'></div>
					<!-- errors will go here -->
				</div>
				<div id='addaddress-group' class='form-group'>
					<div class='col-sm-4 col-sm-offset-3'>
						<div id='addaddress' name='addaddress'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span>Weitere Adresse</div>				
					</div>
					<div class='col-sm-5'></div>
				</div>
				
				<!-- email -->
				<div id='email-group1' class='form-group email-group'>
					<label class='control-label col-sm-3' for='email1'>E-Mail-Adresse (Standard)</label>
					<div class='col-sm-5'>
						<input type='text' class='form-control email' name='email1' id='email1'>
					</div>
					<div class='col-sm-2 emailtype' data-type='standard'></div>
                    <div class='col-sm-2'></div>
					<!-- errors will go here -->
				</div>
				<div id='addemail-group' class='form-group'>
					<div class='col-sm-4 col-sm-offset-3'>
						<div id='addemail' name='addemail'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span>Weitere E-Mail-Adresse</div>				
					</div>
					<div class='col-sm-5'></div>
				</div>
				
				<!-- note -->
				<div id='note-group' class='form-group'>
					<label class='control-label col-sm-3' for='note'>Bemerkungen</label>
					<div class='col-sm-9'>
						<textarea rows='4' class='form-control' name='note' id='note'></textarea>
					</div>
					<!-- errors will go here -->
				</div>
												
				<!-- fields -->
				<div id='fields-group' class='form-group'>
					<label class='control-label col-sm-3' for='fields'>Forschungsgebiete</label>
					<div class='col-sm-9'>
						<select class='form-control' name='fields' id='fields' multiple>
							<option value='1' >Althochdeutsch</option>
							<option value='2' >Anglistik</option>
							<option value='3' >Computerlinguistik</option>
							<option value='4' >Deutsch als Fremdsprache</option>
							<option value='5' >Dialektologie</option>
							<option value='6' >Dialektometrie</option>
							<option value='7' >Friesisch</option>
							<option value='8' >Fr&uuml;hneuhochdeutsch</option>
							<option value='9' >Indogermanisch</option>
							<option value='10' >Jiddistik</option>
							<option value='11' >Klinische Linguistik</option>
							<option value='12' >Lexikographie</option>
							<option value='13' >Lexikologie</option>
							<option value='14' >Luxemburgistik</option>
							<option value='15' >Mittelhochdeutsch</option>
							<option value='16' >Morphologie</option>
							<option value='17' >Niederdeutsch</option>
							<option value='18' >Niederlandistik</option>
							<option value='19' >Oberdeutsch</option>
							<option value='20' >Onomastik</option>
							<option value='21' >Phonetik</option>
							<option value='22' >Phonologie</option>
							<option value='23' >Pragmatik</option>
							<option value='24' >Prosodie</option>
							<option value='25' >Romanistik</option>
							<option value='26' >Semantik</option>
							<option value='27' >Skandinavistik</option>
							<option value='28' >Slavistik</option>
							<option value='29' >Soziolinguistik</option>
							<option value='30' >Sprachdidaktik</option>
							<option value='31' >Sprache und Politik</option>
							<option value='32' >Spracherwerb</option>
							<option value='33' >Sprachgeschichte</option>
							<option value='34' >Sprachinsel</option>
							<option value='35' >Sprachkontakt</option>
							<option value='36' >Sprachphilosophie</option>
							<option value='37' >Sprachtheorie</option>
							<option value='38' >Sprachtypologie</option>
							<option value='39' >Syntax</option>
							<option value='40' >Textlinguistik</option>
							<option value='41' >Variationslinguistik</option>
							<option value='42' >Zimbrisch</option>
						</select>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- published -->
				<div id='published-group' class='form-group'>
					<label class='control-label col-sm-3' for='published'>Ver&ouml;ffentlichungen</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='published' id='published'>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- requests -->
				<div id='requests-group' class='form-group'>
					<label class='control-label col-sm-3' for='requests'>Angefragt</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='requests' id='requests'>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- denied -->
				<div id='denied-group' class='form-group'>
					<label class='control-label col-sm-3' for='denied'>Davon Abgelehnt</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='denied' id='denied'>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- warned -->
				<div id='warned-group' class='form-group'>
					<label class='control-label col-sm-3' for='warned'>Gemahnt</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='warned' id='warned'>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- published_in -->
				<div id='published_in-group' class='form-group'>
					<label class='control-label col-sm-3' for='published_in'>Ver&ouml;ffentlicht in</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='published_in' id='published_in'>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- rate -->
				<div id='rate-group' class='form-group'>
					<label class='control-label col-sm-3' for='rate_content'>Inhaltlich</label>
					<div class='rateit col-sm-4 bigstars' data-rateit-starwidth='32' data-rateit-starheight='32' data-rateit-backingfld='#rate_content'>
						<input type='range' min='0' max='3' value='1.5' step='0.5' class='form-control' name='rate_content' id='rate_content'>
					</div>
					<div class='rateit col-sm-4 bigstars' data-rateit-starwidth='32' data-rateit-starheight='32' data-rateit-backingfld='#rate_form'>
						<input type='range' min='0' max='3' value='1.5' step='0.5' class='form-control' name='rate_form' id='rate_form'>
					</div>
					<label class='control-label col-sm-1' for='rate_form'>Formell</label>
				</div>
				
                <!-- ratenotes -->
				<div id='ratenotes-group' class='form-group'>
					<label class='control-label col-sm-3' for='ratecontent_note'></label>
					<div class='col-sm-4'>
						<textarea rows='4' class='form-control' name='ratecontent_note' id='ratecontent_note'></textarea>
					</div>
					<div class='col-sm-4'>
						<textarea rows='4' class='form-control' name='rateform_note' id='rateform_note'></textarea>
					</div>
					<label class='control-label col-sm-1' for='rateform_note'></label>
				</div>
                
			<button type='submit' class='btn btn-success'>Speichern <span class='glyphicon glyphicon-floppy-disk'></span></button>
			</form>
			
			<form name='fakereviewer' id='fakereviewer' class='form-inline'>
			  <div class='form-group'>
				<label for='fakereviewers'>wieviele Fakerezensenten?</label>
				<select class='form-control' id='fakereviewers' name='fakereviewers'>
				  <option value='1'>1</option>
				  <option value='5'>5</option>
				  <option value='10'>10</option>
				  <option value='20'>20</option>
				  <option value='50'>50</option>
				</select>
			  </div>
			  <button type='submit' class='btn btn-default'>Go!</button>
			</form>			
			";
		echo json_encode($out);
		
	}elseif($_POST["action"] == "newreviewersubmit"){
		$errors         = array();      // array to hold validation errors
		$out            = array();      // array to pass back data

		// validate the variables ======================================================
			// if any of these variables don"t exist, add an error to our $errors array

                        if (empty($_POST["lastname"])) {
                            $errors["lastname"] = "Der Nachname fehlt";
                        }
                        if (empty($_POST["firstname"])) {
                            $errors["firstname"] = "Der Vorname fehlt";
                        }
                        if (empty($_POST["address"]) && empty($_POST["email"])) {
                            $errors["contact"] = "Adressen fehlen";
                        }

		// if there are any errors in our errors array, return a success boolean of false
		if ( ! empty($errors)) {
			// if there are items in our errors array, return those errors
			$out["success"] = false;
			$out["errors"]  = $errors;
		} else {
			$out["html"] = "";
			$newrevsubmit = pg_query("insert into reviewers (
				title, firstname, lastname, gender, published, requests, denied, warned, note, published_in, form, content, formnote, contentnote) 
				values ('" .  $_POST["title"] . "', '" . $_POST["firstname"] . "', '" . 
				$_POST["lastname"] . "', '" . $_POST["gender"] . "', '" . $_POST["published"] .
				"', '" . $_POST["requests"] . "', '" . $_POST["denied"] . "', '" .
				$_POST["warned"] . "', '" . $_POST["note"] . "', '" . $_POST["published_in"] .
				"', " . $_POST["form"] . ", " . $_POST["content"] .
				", '" . $_POST["formnote"] . "', '" . $_POST["contentnote"] . "') returning id");
			$last_rev_id = pg_fetch_all($newrevsubmit)[0]["id"];
			$fields = $_POST["fields"];
			if(isset($_POST["fields"])){
				foreach ($fields as $current_field_id) {
				pg_query('insert into rev_has_fields (revid, fieldid) values (' .  $last_rev_id . ', ' . $current_field_id . ')');
				$out["html"] .= $current_field_id . ' wurde mit ' . $last_rev_id . ' verkn&uuml;pft. <br>';
				}
			}
			$a = 0;
			$address = pg_escape_string(utf8_encode($_POST["address"]));
			$addresstype = $_POST["addresstype"];
			while ( $a < sizeof($address) ){
				pg_query("insert into address (revid, address, type) values (" .  $last_rev_id . ", '" . $address[$a] . "', '" . $addresstype[$a] . "')");
				$out["html"] .= $address[$a] . " wurde mit " . $last_rev_id . " verkn&uuml;pft. <br>";
				$a++;
			}
			$e = 0;
			$email = $_POST["email"];
			$emailtype = $_POST["emailtype"];
			while ( $e < sizeof($address) ){
				pg_query("insert into email (revid, address, type) values (" .  $last_rev_id . ", '" . $email[$e] . "', '" . $emailtype[$e] . "')");
				$out["html"] .= $email[$e] . " wurde mit " . $last_rev_id . " verkn&uuml;pft. <br>";
				$e++;
			}
			// show a message of success and provide a true success variable
			$out["success"] = true;
			$out["message"] = "Success!";
		}
		echo json_encode($out); 
		
	}elseif($_POST["action"] == "fakereviewer"){
		require_once "lib/Faker-master/src/autoload.php";
		$faker = Faker\Factory::create();
		$iw = 0;
		while ($iw < $_POST["value"])	{

			$san_title		= $faker->randomElement($array = array ("Dr.","Prof.","",""));
			$san_firstname	=	pg_escape_string(utf8_encode($faker->firstName));
			$san_lastname 	=	pg_escape_string(utf8_encode($faker->lastName));
			$san_note 		=	pg_escape_string(utf8_encode($faker->unique()->catchPhrase));
			$san_gender		= $faker->randomElement($array = array ("male","female","other"));
			$san_form		= $faker->numberBetween($min = 5, $max = 30);
			$san_content	= $faker->numberBetween($min = 5, $max = 30);
			
			$newrevsubmit = pg_query("insert into reviewers (
				title, firstname, lastname, gender, published, requests, denied, warned, note, published_in, form, content, isblocked) 
				values ('" .  $san_title . "', '" . $san_firstname . "', '" . 
				$san_lastname . "', '" . $san_gender . "', '', '', '', '', " . ($faker->boolean(65) ? "'" . $san_note . "', " : "NULL, ") . " '', " . $san_form . ", " . $san_content . ", FALSE) returning id");
			$last_rev_id = pg_fetch_all($newrevsubmit)[0]["id"];
			$fields = array();
			$if = $faker->numberBetween($min = 2, $max = 5);
			for ($i=1; $i < $if; $i++) {
			  // get a random digit, but always a new one, to avoid duplicates
			  $fields []= $faker->unique()->numberBetween($min = 1, $max = 42);
			}
			foreach ($fields as $current_field_id) {
				pg_query("insert into rev_has_fields (revid, fieldid) values (" .  $last_rev_id . ", " . $current_field_id . ")");
			}
			$a = 0;
			$ia = $faker->numberBetween($min = 2, $max = 5);
			while ( $a < $ia ){
				$san_address 	=	pg_escape_string(utf8_encode($faker->unique()->address));
				$san_addresstype= $faker->randomElement($array = array ("work","private","other"));
				pg_query("insert into address (revid, address, type) values (" .  $last_rev_id . ", '" . $san_address . "', '" . $san_addresstype . "')");
				$a++;
			}
			$e = 0;			
			$ie = $faker->numberBetween($min = 2, $max = 5);
			while ( $e < $ie ){
				$san_email 		=	pg_escape_string(utf8_encode($faker->unique()->safeEmail));
				$san_emailtype	= $faker->randomElement($array = array ("work","private","other"));
				pg_query("insert into email (revid, address, type) values (" .  $last_rev_id . ", '" . $san_email . "', '" . $san_emailtype . "')");
				$e++;
			}
			$iw++;
		}
		$out["response"] = pg_last_error();
		echo json_encode($out);
       
    }elseif($_POST["action"] == "findreviewer"){
		$out["html"] = "
            <form id='findreviewerform' name='findreviewerform' action='backend.php' method='POST' class='form-horizontal' role='form'>	
            
            <!-- name -->
            <div id='name-group' class='form-group'>
                <label class='control-label col-sm-3' for='name'>Name</label>
                <div class='col-sm-5'>
                    <input type='text' class='form-control' name='name' id='name'>
                </div>
                <div class='col-sm-4'></div>
                <!-- errors will go here -->
            </div>
            
			<!-- address -->
			<div id='address-group' class='form-group'>
				<label class='control-label col-sm-3' for='address'>Adresse</label>
				<div class='col-sm-5'>
					<input type='text' class='form-control' name='address' id='address'>
				</div>
				<div class='col-sm-4'></div>
				<!-- errors will go here -->
			</div>
				
            <!-- fields -->
            <div id='fields-group' class='form-group'>
                <label class='control-label col-sm-3' for='fields'>Forschungsgebiete</label>
                <div class='col-sm-9'>
                    <select class='form-control' name='fields' id='fields'>
						<option></option>
                        <option value='1' >Althochdeutsch</option>
                        <option value='2' >Anglistik</option>
                        <option value='3' >Computerlinguistik</option>
                        <option value='4' >Deutsch als Fremdsprache</option>
                        <option value='5' >Dialektologie</option>
                        <option value='6' >Dialektometrie</option>
                        <option value='7' >Friesisch</option>
                        <option value='8' >Fr&uuml;hneuhochdeutsch</option>
                        <option value='9' >Indogermanisch</option>
                        <option value='10' >Jiddistik</option>
                        <option value='11' >Klinische Linguistik</option>
                        <option value='12' >Lexikographie</option>
                        <option value='13' >Lexikologie</option>
                        <option value='14' >Luxemburgistik</option>
                        <option value='15' >Mittelhochdeutsch</option>
                        <option value='16' >Morphologie</option>
                        <option value='17' >Niederdeutsch</option>
                        <option value='18' >Niederlandistik</option>
                        <option value='19' >Oberdeutsch</option>
                        <option value='20' >Onomastik</option>
                        <option value='21' >Phonetik</option>
                        <option value='22' >Phonologie</option>
                        <option value='23' >Pragmatik</option>
                        <option value='24' >Prosodie</option>
                        <option value='25' >Romanistik</option>
                        <option value='26' >Semantik</option>
                        <option value='27' >Skandinavistik</option>
                        <option value='28' >Slavistik</option>
                        <option value='29' >Soziolinguistik</option>
                        <option value='30' >Sprachdidaktik</option>
                        <option value='31' >Sprache und Politik</option>
                        <option value='32' >Spracherwerb</option>
                        <option value='33' >Sprachgeschichte</option>
                        <option value='34' >Sprachinsel</option>
                        <option value='35' >Sprachkontakt</option>
                        <option value='36' >Sprachphilosophie</option>
                        <option value='37' >Sprachtheorie</option>
                        <option value='38' >Sprachtypologie</option>
                        <option value='39' >Syntax</option>
                        <option value='40' >Textlinguistik</option>
                        <option value='41' >Variationslinguistik</option>
                        <option value='42' >Zimbrisch</option>
                    </select>
                </div>
                <!-- errors will go here -->
            </div>
            
			</form>";
		echo json_encode($out);
        
    }elseif($_POST["action"] == "findreviewersubmit"){
		$errors         = array();      // array to hold validation errors
		$out            = array();      // array to pass back data

		// validate the variables ======================================================
			// if any of these variables don"t exist, add an error to our $errors array

                        

		// if there are any errors in our errors array, return a success boolean of false
		if ( ! empty($errors)) {
			// if there are items in our errors array, return those errors
			$out["success"] = false;
			$out["errors"]  = $errors;
		} else {

			// if there are no errors process our form, then return a message

			// DO ALL YOUR FORM PROCESSING HERE
			// THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)
			$out["html"] = "<table class='table table-bordered table-hover sortable'>
			<thead><tr class='info'>
				<th data-defaultsort='asc' data-defaultsign='AZ'>Name:</th>
				<th data-defaultsign='_13'>Inhaltlich:</th>
				<th data-defaultsign='_13'>Formell:</th>
				<th>Bemerkungen:</th> 		
				<th data-defaultsign='_19'>Ver&ouml;ffentlichungen:</th>
				<th data-defaultsign='_19'>Angefragt:</th>
				<th data-defaultsign='_19'>Davon Abgelehnt:</th>
				<th data-defaultsign='_19'>Gemahnt:</th>
				<th>Ver&ouml;ffentlicht in:</th>
				<th>Hat zur Zeit:</th>
			</tr></thead><tbody>";
			$fields = $_POST["fields"];
			$address = $_POST["address"];
			$name = $_POST["name"];	
			if (!empty($_POST["fields"])){
				$result = pg_query("select distinct on (reviewers.id) reviewers.id, reviewers.firstname, reviewers.lastname, reviewers.title, reviewers.published, reviewers.requests, reviewers.denied, reviewers.warned, reviewers.note, reviewers.published_in, reviewers.form, reviewers.content, reviewers.formnote, reviewers.contentnote from reviewers inner join rev_has_fields on rev_has_fields.revid = reviewers.id inner join address on address.revid = reviewers.id where rev_has_fields.fieldid = '" .  $fields . "' and(reviewers.firstname ilike '%" . $name . "%' or reviewers.lastname ilike '%" . $name . "%') and address.address ilike '%" . $address . "%'");
			}else{
				$result = pg_query("select distinct on (reviewers.id) reviewers.id, reviewers.firstname, reviewers.lastname, reviewers.title, reviewers.published, reviewers.requests, reviewers.denied, reviewers.warned, reviewers.note, reviewers.published_in, reviewers.form, reviewers.content, reviewers.formnote, reviewers.contentnote from reviewers inner join address on address.revid = reviewers.id where (reviewers.firstname ilike '%" . $name . "%' or reviewers.lastname ilike '%" . $name . "%') and address.address ilike '%" . $address . "%'");
			}
			while ($b=pg_fetch_row($result)) {
				$out["html"] .= "<tr class='reviewer' data-reviewerid='" . $b[0] . "'>
					<td> " . $b[3] . " " . $b[1] . " " . $b[2] . " " . " </td>
					<td data-value='" . $b[11] . "'><span class='rateit' data-rateit-readonly='true' data-rateit-max='3' data-rateit-value=" . $b[11]/10 . "></span><br>" . $b[13] . "</td>
					<td data-value='" . $b[10] . "'><span class='rateit' data-rateit-readonly='true' data-rateit-max='3' data-rateit-value=" . $b[10]/10 . "></span><br>" . $b[12] . "</td>
					<td>" . $b[8] . "</td>
					<td>" . $b[4] . "</td>
					<td>" . $b[5] . "</td>
					<td>" . $b[6] . "</td>
					<td>" . $b[7] . "</td>
					<td>" . $b[9] . "</td>
					<td>nix</td>
					</tr>";
			}
			
			$out["html"] .= "</tbody></table>";
			// show a message of success and provide a true success variable
			$out["success"] = true;
			$out["message"] = "Success!";
		}
		echo json_encode($out); 
		
	}elseif($_POST["action"] == "reviewers"){
		$out["html"] = "<table class='table table-striped table-hover'>
			<tr class='info'>
				<th>Name:</th>
				<th>Bewertung:<br>
			        (inhaltlich)<br>
					(formell)</th>
				<th>Bemerkungen:</th> 		
			</tr>";
		$all_reviewers = pg_query("select distinct id, title, firstname, lastname, note, form, content from reviewers");
		while ($b=pg_fetch_row($all_reviewers)) {
			$out["html"] .= 
				"<tr>
					<td> " . $b[1] . " " . $b[2] . " " . $b[3] . " " . " </td>
					<td> <span class='rateit' data-rateit-readonly='true' data-rateit-max='3' data-rateit-value=" . $b[6]/10 . "></span><br><span class='rateit' data-rateit-readonly='true' data-rateit-max='3' data-rateit-value=" . $b[5]/10 . "></span></td>
					<td> " . $b[4] . " </td>
				</tr>";
		}
		$out["html"] .= "</table>";
		echo json_encode($out);

	}elseif($_POST["action"] == "revadmin"){
		$out["html2"] = "<div class='modal fade' id='revmodal' tabindex='-1' role='dialog' aria-labelledby='revmodallabel' aria-hidden='true'>
			<div class='modal-dialog modal-lg'>
				<div class='modal-content'>
					<div class='modal-header'>
						<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
						<h4 class='modal-title' id='revmodallabel'>Daten &auml;ndern</h4>
					</div>
					<div class='modal-body'>
						<form class='form-horizontal' role='form'>
			<!-- title -->
				<div id='title-group' class='form-group'>
					<label class='control-label col-sm-3' for='title'>Titel</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='title' id='title'>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- firstname -->
				<div id='firstname-group' class='form-group'>
					<label class='control-label col-sm-3' for='firstname'>Vorname</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='firstname' id='firstname'>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- lastname -->
				<div id='lastname-group' class='form-group'>
					<label class='control-label col-sm-3' for='lastname'>Nachname</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='lastname' id='lastname'>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- gender -->
				<div id='gender-group' class='form-group'>
					<label class='control-label col-sm-3' for='gender'>Geschlecht</label>
					<div class='col-sm-9'>
						<div class='btn-group' data-toggle='buttons'>
							<label for='female' class='btn btn-default'>
								<input type='radio' value='female' name='gender' id='female' autocomplete='off'>
								Weiblich
							</label>
							<label for='male' class='btn btn-default'>
								<input type='radio' value='male' name='gender' id='male' autocomplete='off'>
								M&auml;nnlich
							</label>
							<label for='other' class='btn btn-default'>
								<input type='radio' value='other' name='gender' id='other' autocomplete='off'> Anderes
							</label>
						</div>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- address -->
				<div class='form-group address-group'></div>
				
				<!-- email -->
				<div class='form-group email-group'></div>
				
				<!-- note -->
				<div id='note-group' class='form-group'>
					<label class='control-label col-sm-3' for='note'>Bemerkungen</label>
					<div class='col-sm-9'>
						<textarea rows='4' class='form-control' name='note' id='note'></textarea>
					</div>
					<!-- errors will go here -->
				</div>
												
				<!-- fields -->
				<div id='fields-group' class='form-group'>
					<label class='control-label col-sm-3' for='fields'>Forschungsgebiete</label>
					<div class='col-sm-9'>
						<select style='width: 100%;' class='form-control' name='fields' id='fields' multiple>
							<option value='1' >Althochdeutsch</option>
							<option value='2' >Anglistik</option>
							<option value='3' >Computerlinguistik</option>
							<option value='4' >Deutsch als Fremdsprache</option>
							<option value='5' >Dialektologie</option>
							<option value='6' >Dialektometrie</option>
							<option value='7' >Friesisch</option>
							<option value='8' >Fr&uuml;hneuhochdeutsch</option>
							<option value='9' >Indogermanisch</option>
							<option value='10' >Jiddistik</option>
							<option value='11' >Klinische Linguistik</option>
							<option value='12' >Lexikographie</option>
							<option value='13' >Lexikologie</option>
							<option value='14' >Luxemburgistik</option>
							<option value='15' >Mittelhochdeutsch</option>
							<option value='16' >Morphologie</option>
							<option value='17' >Niederdeutsch</option>
							<option value='18' >Niederlandistik</option>
							<option value='19' >Oberdeutsch</option>
							<option value='20' >Onomastik</option>
							<option value='21' >Phonetik</option>
							<option value='22' >Phonologie</option>
							<option value='23' >Pragmatik</option>
							<option value='24' >Prosodie</option>
							<option value='25' >Romanistik</option>
							<option value='26' >Semantik</option>
							<option value='27' >Skandinavistik</option>
							<option value='28' >Slavistik</option>
							<option value='29' >Soziolinguistik</option>
							<option value='30' >Sprachdidaktik</option>
							<option value='31' >Sprache und Politik</option>
							<option value='32' >Spracherwerb</option>
							<option value='33' >Sprachgeschichte</option>
							<option value='34' >Sprachinsel</option>
							<option value='35' >Sprachkontakt</option>
							<option value='36' >Sprachphilosophie</option>
							<option value='37' >Sprachtheorie</option>
							<option value='38' >Sprachtypologie</option>
							<option value='39' >Syntax</option>
							<option value='40' >Textlinguistik</option>
							<option value='41' >Variationslinguistik</option>
							<option value='42' >Zimbrisch</option>
						</select>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- published -->
				<div id='published-group' class='form-group'>
					<label class='control-label col-sm-3' for='published'>Ver&ouml;ffentlichungen</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='published' id='published'>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- requests -->
				<div id='requests-group' class='form-group'>
					<label class='control-label col-sm-3' for='requests'>Angefragt</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='requests' id='requests'>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- denied -->
				<div id='denied-group' class='form-group'>
					<label class='control-label col-sm-3' for='denied'>Davon Abgelehnt</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='denied' id='denied'>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- warned -->
				<div id='warned-group' class='form-group'>
					<label class='control-label col-sm-3' for='warned'>Gemahnt</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='warned' id='warned'>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- published_in -->
				<div id='published_in-group' class='form-group'>
					<label class='control-label col-sm-3' for='published_in'>Ver&ouml;ffentlicht in</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='published_in' id='published_in'>
					</div>
					<!-- errors will go here -->
				</div>
				
				<!-- rate -->
				<div id='rate-group' class='form-group'>
					<label class='control-label col-sm-3' for='rate_content'>Inhaltlich</label>
					<div id='rate_content_div' class='rateit col-sm-3 bigstars' data-rateit-starwidth='32' data-rateit-starheight='32' data-rateit-min='0' data-rateit-max='3' data-rateit-step='0.5'>
					</div>
					<div id='rate_form_div' class='rateit col-sm-3 bigstars' data-rateit-starwidth='32' data-rateit-starheight='32' data-rateit-min='0' data-rateit-max='3' data-rateit-step='0.5'>
					</div>
					<label class='control-label col-sm-1' for='rate_form'>Formell</label>
				</div>
				
                <!-- ratenotes -->
				<div id='ratenotes-group' class='form-group'>
					<label class='control-label col-sm-3' for='ratecontent_note'></label>
					<div class='col-sm-4'>
						<textarea rows='4' class='form-control' name='ratecontent_note' id='ratecontent_note'></textarea>
					</div>
					<label class='control-label col-sm-1' for='rateform_note'></label>
					<div class='col-sm-4'>
						<textarea rows='4' class='form-control' name='rateform_note' id='rateform_note'></textarea>
					</div>
				</div>
			
				</form>
					</div>
					<div class='modal-footer'>
						<button type='button' id='deleterev' name='deleterev' class='btn btn-danger pull-left'><span class='glyphicon glyphicon-trash'></span>L&ouml;schen</button>
						<button type='button' id='findreviewmodal' name='findreviewmodal' class='btn btn-primary'>Rezensionen <span class='glyphicon glyphicon-floppy-disk'></span></button>
						<button type='button' id='updaterev' name='updaterev' class='btn btn-success'>Speichern <span class='glyphicon glyphicon-floppy-disk'></span></button>
					</div>
				</div>
			</div>
		</div>
			<form class='form-horizontal' role='form'>
			<!-- namesearch -->
            <div id='namesearch-group' class='form-group'>
                <label class='control-label col-sm-3' for='namesearch'>Name</label>
                <div class='col-sm-5'>
                    <input type='text' class='form-control' name='namesearch' id='namesearch'>
                </div>
                <div class='col-sm-4'></div>
                <!-- errors will go here -->
            </div>
            
			<!-- addresssearch -->
			<div id='addresssearch-group' class='form-group'>
				<label class='control-label col-sm-3' for='addresssearch'>Adresse</label>
				<div class='col-sm-5'>
					<input type='text' class='form-control' name='addresssearch' id='addresssearch'>
				</div>
				<div class='col-sm-4'></div>
				<!-- errors will go here -->
			</div>
				
            <!-- fieldssearch -->
            <div id='fieldssearch-group' class='form-group'>
                <label class='control-label col-sm-3' for='fieldssearch'>Forschungsgebiete</label>
                <div class='col-sm-9'>
                    <select class='form-control' name='fieldssearch' id='fieldssearch'>
						<option></option>
                        <option value='1' >Althochdeutsch</option>
                        <option value='2' >Anglistik</option>
                        <option value='3' >Computerlinguistik</option>
                        <option value='4' >Deutsch als Fremdsprache</option>
                        <option value='5' >Dialektologie</option>
                        <option value='6' >Dialektometrie</option>
                        <option value='7' >Friesisch</option>
                        <option value='8' >Fr&uuml;hneuhochdeutsch</option>
                        <option value='9' >Indogermanisch</option>
                        <option value='10' >Jiddistik</option>
                        <option value='11' >Klinische Linguistik</option>
                        <option value='12' >Lexikographie</option>
                        <option value='13' >Lexikologie</option>
                        <option value='14' >Luxemburgistik</option>
                        <option value='15' >Mittelhochdeutsch</option>
                        <option value='16' >Morphologie</option>
                        <option value='17' >Niederdeutsch</option>
                        <option value='18' >Niederlandistik</option>
                        <option value='19' >Oberdeutsch</option>
                        <option value='20' >Onomastik</option>
                        <option value='21' >Phonetik</option>
                        <option value='22' >Phonologie</option>
                        <option value='23' >Pragmatik</option>
                        <option value='24' >Prosodie</option>
                        <option value='25' >Romanistik</option>
                        <option value='26' >Semantik</option>
                        <option value='27' >Skandinavistik</option>
                        <option value='28' >Slavistik</option>
                        <option value='29' >Soziolinguistik</option>
                        <option value='30' >Sprachdidaktik</option>
                        <option value='31' >Sprache und Politik</option>
                        <option value='32' >Spracherwerb</option>
                        <option value='33' >Sprachgeschichte</option>
                        <option value='34' >Sprachinsel</option>
                        <option value='35' >Sprachkontakt</option>
                        <option value='36' >Sprachphilosophie</option>
                        <option value='37' >Sprachtheorie</option>
                        <option value='38' >Sprachtypologie</option>
                        <option value='39' >Syntax</option>
                        <option value='40' >Textlinguistik</option>
                        <option value='41' >Variationslinguistik</option>
                        <option value='42' >Zimbrisch</option>
                    </select>
                </div>
                <!-- errors will go here -->
            </div>
			</form>";
		$out["html"] = "
			<table class='added table table-bordered table-hover sortable'>
			<thead><tr class='info'>
				<th data-defaultsort='asc' data-defaultsign='AZ'>Name:</th>
				<th data-defaultsign='_13'>Inhaltlich:</th>
				<th data-defaultsign='_13'>Formell:</th>
				<th>Bemerkungen:</th> 		
				<th data-defaultsign='_19'>Ver&ouml;ffentlichungen:</th>
				<th data-defaultsign='_19'>Angefragt:</th>
				<th data-defaultsign='_19'>Davon Abgelehnt:</th>
				<th data-defaultsign='_19'>Gemahnt:</th>
				<th>Ver&ouml;ffentlicht in:</th>
				<th>Hat zur Zeit:</th>
			</tr></thead><tbody>";
		$fields = $_POST["fields"];
		$address = pg_escape_string(utf8_encode($_POST["address"]));
		$name = pg_escape_string(utf8_encode($_POST["name"]));	
		if (!empty($_POST["fields"])){
			$result = pg_query("select distinct on (reviewers.id) reviewers.id, reviewers.firstname, reviewers.lastname, reviewers.title, reviewers.published, reviewers.requests, reviewers.denied, reviewers.warned, reviewers.note, reviewers.published_in, reviewers.form, reviewers.content, reviewers.formnote, reviewers.contentnote from reviewers inner join rev_has_fields on rev_has_fields.revid = reviewers.id inner join address on address.revid = reviewers.id where rev_has_fields.fieldid = '" .  $fields . "' and(reviewers.firstname ilike '%" . $name . "%' or reviewers.lastname ilike '%" . $name . "%') and address.address ilike '%" . $address . "%'");
		}else{
			$result = pg_query("select distinct on (reviewers.id) reviewers.id, reviewers.firstname, reviewers.lastname, reviewers.title, reviewers.published, reviewers.requests, reviewers.denied, reviewers.warned, reviewers.note, reviewers.published_in, reviewers.form, reviewers.content, reviewers.formnote, reviewers.contentnote from reviewers inner join address on address.revid = reviewers.id where (reviewers.firstname ilike '%" . $name . "%' or reviewers.lastname ilike '%" . $name . "%') and address.address ilike '%" . $address . "%'");
		}
		while ($b=pg_fetch_row($result)) {
			$out["html"] .= "<tr class='reviewer' data-toggle='modal' data-target='#revmodal' data-reviewerid='" . $b[0] . "'>
				<td> " . $b[3] . " " . $b[1] . " " . $b[2] . " " . " </td>
				<td data-value='" . $b[11] . "'><span class='rateit' data-rateit-readonly='true' data-rateit-max='3' data-rateit-value=" . $b[11]/10 . "></span><br>" . $b[13] . "</td>
				<td data-value='" . $b[10] . "'><span class='rateit' data-rateit-readonly='true' data-rateit-max='3' data-rateit-value=" . $b[10]/10 . "></span><br>" . $b[12] . "</td>
				<td>" . $b[8] . "</td>
				<td>" . $b[4] . "</td>
				<td>" . $b[5] . "</td>
				<td>" . $b[6] . "</td>
				<td>" . $b[7] . "</td>
				<td>" . $b[9] . "</td>
				<td>nix</td>
				</tr>";
		}	
		$out["html"] .= "</tbody></table>";
		echo json_encode($out);
		
	}elseif($_POST["action"] == "revdata"){
		$reviewer = pg_query("select * from reviewers where id = " . $_POST["revid"]);
		while ($b=pg_fetch_row($reviewer)){
			$out["id"] = $b[0];
			$out["firstname"] = $b[1];
			$out["lastname"] = $b[2];
			$out["title"] = $b[3];
			$out["gender"] = $b[4];
			$out["published"] = $b[5];
			$out["requests"] = $b[6];
			$out["denied"] = $b[7];
			$out["warned"] = $b[8];
			$out["note"] = $b[9];
			$out["published_in"] = $b[10];
			$out["form"] = $b[11];
			$out["content"] = $b[12];
			$out["formnote"] = $b[13];
			$out["contentnote"] = $b[14];
		}
		$address = pg_query("select address, type from address where revid = " . $_POST["revid"]);
		$addressarray = array();
		while ($a=pg_fetch_row($address)){
			$addressarray[] = $a;
		}
		$out["address"] = $addressarray;
		$email = pg_query("select address, type from email where revid = " . $_POST["revid"]);
		$emailarray = array();
		while ($e=pg_fetch_row($email)){
			$emailarray[] = $e;
		}
		$out["email"] = $emailarray;
		$fields = pg_query("select * from rev_has_fields where revid = " . $_POST["revid"]);
		$fieldarray = "";
		while ($f=pg_fetch_row($fields)){
			$fieldarray .= $f[1] . ",";
		}
		$fieldarray = rtrim($fieldarray, ",");
		//$fieldarray .= "]";
		$out["fields"] = $fieldarray;
		echo json_encode($out);
/*
	}elseif($_POST["action"] == "bookupdate"){
		$title = pg_escape_string(utf8_encode($_POST["title"]));
		$author = pg_escape_string(utf8_encode($_POST["author"]));
		$publisher = pg_escape_string(utf8_encode($_POST["publisher"]));
		$pub_country = pg_escape_string(utf8_encode($_POST["pub_country"]));
		$price = pg_escape_string(utf8_encode($_POST["price"]));
		$note = pg_escape_string(utf8_encode($_POST["note"]));
		$pages = pg_escape_string(utf8_encode($_POST["pages"]));
		$series = pg_escape_string(utf8_encode($_POST["series"]));
		$nr_in_series = pg_escape_string(utf8_encode($_POST["nr_in_series"]));
		$pub_date = pg_escape_string(utf8_encode($_POST["pub_date"]));
		$isbn = pg_escape_string(utf8_encode($_POST["isbn"]));

		$result = pg_query("update books set (title, author, publisher, pub_country, price, note, 
				pages, series, nr_in_series, pub_date, isbn) = ('" .  $title . "', '" . $author . "', '" . 
				$publisher . "', '" . $pub_country . "', '" . 
				$price . "', '" . $note . "', '" . $pages . "', '" . 
				$series . "', '" . $nr_in_series . "', '" . $pub_date . "', '" . 
				$isbn . "') where id = " . $_POST["bookid"]);
		$out["response"] = pg_last_error();
		echo json_encode($out);
*/		
	}elseif($_POST["action"] == "revdelete"){
		$result = pg_query("delete from reviewers where id = " . $_POST["revid"]);
		$out["response"] = pg_last_error();
		echo json_encode($out);
   
    }elseif($_POST["action"] == "newreview"){
		$out["html"] = "
            <form id='findreviewerform' name='findreviewerform' action='backend.php' method='POST' class='form-horizontal' role='form'>	
            
            <!-- name -->
            <div id='name-group' class='form-group'>
                <label class='control-label col-sm-3' for='name'>Name</label>
                <div class='col-sm-6'>
                    <input type='text' class='form-control' name='name' id='name'>
                </div>
                <div class='col-sm-3'>
                    oder
                </div>
                <!-- errors will go here -->
            </div>
                
            <!-- fields -->
            <div id='fields-group' class='form-group'>
                <label class='control-label col-sm-3' for='fields'>Forschungs-gebiete</label>
                <div class='col-sm-9'>
                    <select class='form-control' name='fields' id='fields'>
                        <option value='1' >Althochdeutsch</option>
                        <option value='2' >Anglistik</option>
                        <option value='3' >Computerlinguistik</option>
                        <option value='4' >Deutsch als Fremdsprache</option>
                        <option value='5' >Dialektologie</option>
                        <option value='6' >Dialektometrie</option>
                        <option value='7' >Friesisch</option>
                        <option value='8' >Fr&uuml;hneuhochdeutsch</option>
                        <option value='9' >Indogermanisch</option>
                        <option value='10' >Jiddistik</option>
                        <option value='11' >Klinische Linguistik</option>
                        <option value='12' >Lexikographie</option>
                        <option value='13' >Lexikologie</option>
                        <option value='14' >Luxemburgistik</option>
                        <option value='15' >Mittelhochdeutsch</option>
                        <option value='16' >Morphologie</option>
                        <option value='17' >Niederdeutsch</option>
                        <option value='18' >Niederlandistik</option>
                        <option value='19' >Oberdeutsch</option>
                        <option value='20' >Onomastik</option>
                        <option value='21' >Phonetik</option>
                        <option value='22' >Phonologie</option>
                        <option value='23' >Pragmatik</option>
                        <option value='24' >Prosodie</option>
                        <option value='25' >Romanistik</option>
                        <option value='26' >Semantik</option>
                        <option value='27' >Skandinavistik</option>
                        <option value='28' >Slavistik</option>
                        <option value='29' >Soziolinguistik</option>
                        <option value='30' >Sprachdidaktik</option>
                        <option value='31' >Sprache und Politik</option>
                        <option value='32' >Spracherwerb</option>
                        <option value='33' >Sprachgeschichte</option>
                        <option value='34' >Sprachinsel</option>
                        <option value='35' >Sprachkontakt</option>
                        <option value='36' >Sprachphilosophie</option>
                        <option value='37' >Sprachtheorie</option>
                        <option value='38' >Sprachtypologie</option>
                        <option value='39' >Syntax</option>
                        <option value='40' >Textlinguistik</option>
                        <option value='41' >Variationslinguistik</option>
                        <option value='42' >Zimbrisch</option>
                    </select>
                </div>
                <!-- errors will go here -->
            </div>
            
            <div id='submit-group' class='form-group'>
                <div class='col-sm-3 col-sm-offset-3'>
                    <button type='submit' class='btn btn-success'>Suchen <span class='glyphicon glyphicon-floppy-disk'></span></button>
                </div>
                <!-- errors will go here -->
            </div>
            
			</form>";
		echo json_encode($out);
        
    }elseif($_POST["action"] == "addreview"){
		$result1 = pg_query("insert into reviews (bookid, revid) values ('" . $_POST["bookid"] . "', '" . $_POST["revid"] . "') returning id");
		$reviewid = pg_fetch_all($result1)[0]["id"];
		$result2 = pg_query("insert into events (reviewid, type, timestamp, revid) values ('" . $reviewid . "', 'Neue Rezension', 'now' , '" . $_POST["revid"] . "')");
		$out["success"] = true;
		$out["message"] = "Success!";
		echo json_encode($out); 

    }elseif($_POST["action"] == "reviews"){
		$errors         = array();      // array to hold validation errors
		$out            = array();      // array to pass back data
		
		if ( ! empty($errors)) {
			// if there are items in our errors array, return those errors
			$out["success"] = false;
			$out["errors"]  = $errors;
		} else {
			$out["html2"] = "<div class='modal fade' id='reviewmodal' tabindex='-1' role='dialog' aria-labelledby='rev_request_nav' aria-hidden='true'>
			<div class='modal-dialog modal-lg'>
				<div class='modal-content'>
					<div class='modal-header'>
						<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
						<ul class='nav nav-tabs' id='tabContent'>
    <li class='active'><a href='#rev_decline' data-toggle='tab'><h4 class='modal-title' id='rev_decline_nav'>Absage</h4></a></li>
	<li><a href='#rev_deadline' data-toggle='tab'><h4 class='modal-title' id='rev_deadline_nav'>Deadline</h4></a></li>
	<li><a href='#rev_warning' data-toggle='tab'><h4 class='modal-title' id='rev_warning_nav'>Mahnung</h4></a></li>
	<li><a href='#rev_change' data-toggle='tab'><h4 class='modal-title' id='rev_change_nav'>Wechsel</h4></a></li>
	<li><a href='#rev_zdl' data-toggle='tab'><h4 class='modal-title' id='rev_zdl_nav'>ZDL</h4></a></li>
</ul>
					</div>
					<div class='modal-body tab-content'>
						<div class='tab-pane active' id='rev_decline'>
							<label for='rev_decline_reason'>Begr&uuml;ndung:</label><br>
							<textarea rows='4' name='rev_decline_reason' id='rev_decline_reason'></textarea><br><br>
							<button type='button' id='rev_decline_btn' name='rev_decline_btn' class='btn btn-primary'>Rezensent hat abgesagt</button>
						</div>
						<div class='tab-pane' id='rev_deadline'>
							<label for='rev_deadline_date'>Datum:</label><br>
							<input id='rev_deadline_date' name='rev_deadline_date'></input><br><br>
							<label for='rev_deadline_note'>Kommentar:</label><br>
							<textarea rows='4' name='rev_deadline_note' id='rev_deadline_note'></textarea><br><br>
							<button type='button' id='rev_deadline_btn' name='rev_deadline_btn' class='btn btn-primary'>Deadline festlegen</button>
						</div>
						<div class='tab-pane' id='rev_warning'>
							<label for='rev_warning_note'>Kommentar:</label><br>
							<textarea rows='4' name='rev_warning_note' id='rev_warning_note'></textarea><br><br>
							<button type='button' id='rev_warning_btn' name='rev_warning_btn' class='btn btn-primary'>Rezensent wurde gemahnt</button>
						</div>
						<div class='tab-pane' id='rev_change'>
							<label for='rev_change_newrev'>Neuer Rezensent:</label><br>
							<input type='text' id='rev_change_newrev' name='rev_change_newrev'><br><br>
							<label for='rev_change_note'>Kommentar:</label><br>
							<textarea rows='4' name='rev_change_note' id='rev_change_note'></textarea><br><br>
							<button type='button' id='rev_change_btn' name='rev_change_btn' class='btn btn-primary'>Rezensent hat gewechselt</button><br>
						</div>
						<div class='tab-pane' id='rev_zdl'>
							<span class='cb_container'><input type='checkbox' id='review_arrived' name='review_arrived'> Rezension ist angekommen, </span>Datum: <input id='review_arrived_date' name='review_arrived_date'><br><br>
							Dateispeicherort: <input type='text' id='rev_filelocation' name='rev_filelocation' size='3'> Kommentar: <textarea id='arrived_note' name='arrived_note'></textarea><br><br>
							<span class='cb_container'><input type='checkbox' id='formatiert' name='formatiert'> Formatiert, </span>Kommentar: <textarea id='formatiert_note' name='formatiert_note'></textarea><br><br>
							<span class='cb_container'><input type='checkbox' id='lektoriert' name='lektoriert'> Lektoriert, </span>Kommentar: <textarea id='lektoriert_note' name='lektoriert_note'></textarea><br><br>
							<span class='cb_container'><input type='checkbox' id='zum_setzen' name='zum_setzen'> zum Setzen, </span>Kommentar: <textarea id='zum_setzen_note' name='zum_setzen_note'></textarea><br><br>
							<span class='cb_container'><input type='checkbox' id='vom_setzen' name='vom_setzen'> vom Setzen zur&uuml;ck, </span>Anzahl Seiten: <input id='vom_setzen_seitenzahl' name='vom_setzen_seitenzahl' size='3'></input> Kommentar: <textarea id='vom_setzen_note' name='vom_setzen_note'></textarea><br><br>
							<span class='cb_container'><input type='checkbox' id='imprimatur' name='imprimatur'> Imprimatur erteilt, </span>Kommentar: <textarea id='imprimatur_note' name='imprimatur_note'></textarea><br><br>
							<span class='cb_container'><input type='checkbox' id='umbruch' name='umbruch'> Umbruch angekommen, </span>Kommentar: <textarea id='umbruch_note' name='umbruch_note'></textarea><br><br>
							<button id='rev_zdl_btn' name='rev_zdl_btn' class='btn btn-success'>Speichern <span class='glyphicon glyphicon-floppy-disk'></span></button>
						</div>
					</div>
					<div class='modal-footer'>
					</div>
				</div>
			</div>
		</div>
			 <form id='findreviewform' name='findreviewform' method='POST' class='form-horizontal' role='form'>	
			<!-- name -->
            <div id='name-group' class='form-group'>
                <label class='control-label col-sm-3' for='name'>Name</label>
                <div class='col-sm-5'>
                    <input type='text' class='form-control reviewsearch' name='name' id='name'>
                </div>
                <div class='col-sm-4'></div>
                <!-- errors will go here -->
            </div>
			<!-- book -->
            <div id='book-group' class='form-group'>
                <label class='control-label col-sm-3' for='book'>Buch</label>
                <div class='col-sm-5'>
                    <input type='text' class='form-control reviewsearch' name='book' id='book'>
                </div>
                <div class='col-sm-4'></div>
                <!-- errors will go here -->
            </div></form>";
			$out["html"] = "
			<table class='table added table-bordered table-hover'>
			<thead><tr class='info'>
				<th>Buch:</th>
				<th>Rezensent:</th>
				<th>Aktuell:</th>
				<th><button id='batch_zum_setzen' name='batch_zum_setzen' class='btn'>Zum Setzen</button><br>
				<button id='batch_vom_setzen' name='batch_vom_setzen' class='btn'>Vom Setzen</button><br>
				<button id='batch_heft_zuordnung' name='batch_heft_zuordnung' class='btn'>Heft zuordnen</button><br></th>
			</tr></thead><tbody>";
			$name = pg_escape_string(utf8_encode($_POST["name"]));
			$book = pg_escape_string(utf8_encode($_POST["book"]));
			if(empty($_POST["name"]) && empty($_POST["book"])){
				$result = pg_query("with nev as (select e1.* from events as e1 left join events as e2 on e1.reviewid=e2.reviewid and e1.timestamp < e2.timestamp where e2.timestamp is null) select reviews.id, nev.type, nev.timestamp, nev.date, nev.payload, books.title, reviewers.title, reviewers.firstname, reviewers.lastname from nev inner join reviews on nev.reviewid=reviews.id inner join books on reviews.bookid=books.id left outer join reviewers on reviews.revid=reviewers.id order by nev.timestamp asc");
			}elseif(empty($_POST["name"])){
				$result = pg_query("with nev as (select e1.* from events as e1 left join events as e2 on e1.reviewid=e2.reviewid and e1.timestamp < e2.timestamp where e2.timestamp is null) select reviews.id, nev.type, nev.timestamp, nev.date, nev.payload, books.title, reviewers.title, reviewers.firstname, reviewers.lastname from nev inner join reviews on nev.reviewid=reviews.id inner join books on reviews.bookid=books.id left outer join reviewers on reviews.revid=reviewers.id where books.title ilike '%" . $book . "%' order by nev.timestamp asc");
			}else{
				$result = pg_query("with nev as (select e1.* from events as e1 left join events as e2 on e1.reviewid=e2.reviewid and e1.timestamp < e2.timestamp where e2.timestamp is null) select reviews.id, nev.type, nev.timestamp, nev.date, nev.payload, books.title, reviewers.title, reviewers.firstname, reviewers.lastname from nev inner join reviews on nev.reviewid=reviews.id inner join books on reviews.bookid=books.id left outer join reviewers on reviews.revid=reviewers.id where books.title ilike '%" . $book . "%' and (reviewers.firstname ilike '%" . $name . "%' or reviewers.lastname ilike '%" . $name . "%') order by nev.timestamp asc");

			}
			while ($b=pg_fetch_row($result)) {
				$dt = DateTime::createFromFormat('Y-m-d H:i:s.u', $b[2]);
				$date = $dt->format('d.m.y');
				$deadline = "";
				$note = "";
				if (!empty($b[3])){
					$deadline = " | Termin: " . $b[3];
				}
				if (!empty($b[4])){
					$note = " | Bemerkung: " . $b[4];
				}
				$out["html"] .= "<tr>
					<td class='accordion-toggle' data-toggle='collapse' data-target='#event" . $b[0] . "'> " . $b[5] . "</td>
					<td class='accordion-toggle' data-toggle='collapse' data-target='#event" . $b[0] . "'> " . $b[6] . " " . $b[7] . " " . $b[8] . " " . " </td>
					<td class='accordion-toggle' data-toggle='collapse' data-target='#event" . $b[0] . "'> Typ: " . $b[1] . " | vom: " . $date . $deadline . $note . "</td>
					<td class='cb_container' align='center' valign='middle'><input type='checkbox' id='review_select_" . $b[0] . "' name='review_select_" . $b[0] . "'></td>
					</tr>
					<tr><td colspan='3' data-toggle='modal' data-target='#reviewmodal' class='hiddenRow accordion-body collapse event active' id='event" . $b[0] . "' data-reviewid='" . $b[0] . "'><div class='alert'><table>";
				$events = pg_query("select * from events where reviewid=" . $b[0] . " order by events.timestamp desc");
				while($c=pg_fetch_row($events)){
					$dt2 = DateTime::createFromFormat('Y-m-d H:i:s.u', $c[2]);
					$date2 = $dt2->format('d.m.y');
					$deadline2 = "";
					$note2 = "";
					if (!empty($c[3])){
						$deadline2 = " | Datum: " . $c[3];
					}
					if (!empty($c[5])){
						$note2 = " | Bemerkung: " . $c[5];
					}
					$out["html"] .= "<tr>
						<td>
							<div> Vorgang: " . $c[1] . " | vom: " . $date2 . $deadline2 . $note2 . "</div>
						</td>
					</tr>";
				}
				$out["html"] .= "</table></div></td></tr>";
			}
			
			$out["html"] .= "</tbody></table>";
			// show a message of success and provide a true success variable
			$out["success"] = true;
			$out["message"] = "Success!";
		}
		echo json_encode($out); 

    }else{
		$out["html"] = "nicht im backend gefunden";
		echo json_encode($out);
	}
	//echo json_encode($out);
	//TODO: Input sanitizing(pg_escape_string(), pg_query_params()) - fields: freie tags - Rezensenten blockieren - 
